
package ua.com.driverTrip.gson.updateTripResp;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdateDriverTripResp implements Serializable
{

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("data")
    @Expose
    private Data data;
    private final static long serialVersionUID = 479954865688056804L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public UpdateDriverTripResp() {
    }

    /**
     * 
     * @param data
     * @param success
     */
    public UpdateDriverTripResp(Boolean success, Data data) {
        super();
        this.success = success;
        this.data = data;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

}
