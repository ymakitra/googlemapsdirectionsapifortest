package ua.com.driverTrip;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import ua.com.Url;
import ua.com.Variables;
import ua.com.driverTrip.gson.updateTripResp.UpdateDriverTripResp;
import ua.com.service.RandomGenerator;
import ua.com.staticScenarios.StaticVariables;

import static io.restassured.RestAssured.given;

public class DriverTrip {
    public static int CreateTrip(String startLat, String startLng, String endLat, String endLng, String loginToken, int carId, String deviceToken) {
        String url = Url.base;
        String urlCreateTrip = Url.driverTrip;

        Variables variables = new Variables();
        int distance = RandomGenerator.IntRandomGenerator(variables.getDistanceMin(), variables.getDistanceMax());
        String distanceS = Integer.toString(distance);

        Response Post;
        Post = given().
                relaxedHTTPSValidation().
                header("Authorization", ("Bearer " + loginToken)).
                header("X-Device-Token", deviceToken).
                param("car_id", carId).
                param("start_pos", (startLat + ", " + startLng)).
                param("finish_pos", (endLat + ", " + endLng)).
                param("start_location", "Green Str").
                param("finish_location", "Red Str").
                param("distance", distanceS).
                expect().
                statusCode(201).
//                log().all().
        when().
                        post(url + urlCreateTrip).
                        then().
                        extract().
                        response();

        //        GET TRIP ID FROM RESPONSE
        //        System.out.println(startLat);
        return Post.path("data.id");
    }

    public static void FinishDriverTrip(String loginToken, String deviceToken, int tripId) {
        String url = Url.base;
        String urlTrip = Url.driverTrips;
        String urlFinish = Url.finishTrip;

        given().
                relaxedHTTPSValidation().
                header("Authorization", ("Bearer " + loginToken)).
                header("X-Device-Token", deviceToken).
                param("id", tripId).
                expect().
                statusCode(200).
//                log().all().
        when().
                post(url + urlTrip + tripId + urlFinish).
                then().
                extract().
                response();
    }

    public static int GetActiveTripForDriver(String loginToken, String deviceToken) {
        String url = Url.base;
        String urlTrip = Url.activeTrip;

        Response Post;
        Post = given().
                relaxedHTTPSValidation().
                header("Authorization", ("Bearer " + loginToken)).
                header("X-Device-Token", deviceToken).
                expect().
                statusCode(200).
//                log().all().
                when().
                get(url + urlTrip).
                then().
                extract().
                response();
//        System.out.println("Trip Id: "    + Post.path("data.id"));
        return Post.path("data.id");
    }

    public static void GetActiveTripForDriverFail(String loginToken, String deviceToken) {
        String url = Url.base;
        String urlTrip = Url.activeTrip;

                given().
                relaxedHTTPSValidation().
                header("Authorization", ("Bearer " + loginToken)).
                header("X-Device-Token", deviceToken).
                expect().
                statusCode(404).
//                log().all().
                when().
                get(url + urlTrip).
                then().
                extract().
                response();
    }


    public static void AllDriverTrips(String loginToken, String deviceToken) {
        String url = Url.base;
        String urlTrip = Url.driverTrip;

        given().
                relaxedHTTPSValidation().
                header("Authorization", ("Bearer " + loginToken)).
                header("X-Device-Token", deviceToken).
                expect().
                statusCode(200).
//                log().all().
        when().
                get(url + urlTrip).
                then().
                extract().
                response();
    }

    public static void GetDriverTrip(String loginToken, String deviceToken, int tripId) {
        String url = Url.base;
        String urlTrip = Url.driverTrips;

        given().
                relaxedHTTPSValidation().
                header("Authorization", ("Bearer " + loginToken)).
                header("X-Device-Token", deviceToken).
                expect().
                statusCode(200).
//                log().all().
        when().
                get(url + urlTrip + tripId).
                then().
                extract().
                response();
    }

    public static void GetIntersectedTrips(String loginToken, String deviceToken) {
        String url = Url.base;
        String urlTrip = Url.driverTrips;

        given().
                relaxedHTTPSValidation().
                header("Authorization", ("Bearer " + loginToken)).
                header("X-Device-Token", deviceToken).
                expect().
                statusCode(200).
//                log().all().
                when().
                get(url + urlTrip + "intersect?start_pos=" + StaticVariables.startLng + "," + StaticVariables.startLat + "&finish_pos=" + StaticVariables.endLng + "," + StaticVariables.endLat).
                then().
                extract().
                response();
    }

    public UpdateDriverTripResp UpdateDriverTrip(String loginToken, String deviceToken, int Id, int carId) {
        String url = Url.base;
        String urlTrip = Url.driverTrips;

        DriverTripInfo putTrip = new DriverTripInfo();
        putTrip.setId(Id);
        putTrip.setCar_id(carId);
        putTrip.setStart_pos(StaticVariables.startLng2 + "," + StaticVariables.startLat2);
        putTrip.setFinish_pos(StaticVariables.endLng2 + "," + StaticVariables.endLat2);
        putTrip.setStart_location(StaticVariables.address1);
        putTrip.setFinish_location(StaticVariables.address2);
        putTrip.setDistance(1234);
        putTrip.setSeats(9);

        Response response;
        response = given().
                relaxedHTTPSValidation().
                header("Authorization", ("Bearer " + loginToken)).
                header("X-Device-Token", deviceToken).
                body(putTrip).
                contentType(ContentType.JSON).
                expect().
                statusCode(200).
//                log().all().
                when().
//                contentType(ContentType.JSON).
                put(url + urlTrip + Id);

        UpdateDriverTripResp dataFromResp = response.as(UpdateDriverTripResp.class);
        return dataFromResp;
    }

    public class DriverTripInfo {
        private int id;
        private int car_id;
        private String start_pos;
        private String finish_pos;
        private String start_location;
        private String finish_location;
        private int distance;
        private int seats;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        void setCar_id(int car_id) {
            this.car_id = car_id;
        }

        void setStart_pos(String start_pos) {
            this.start_pos = start_pos;
        }

        void setFinish_pos(String finish_pos) {
            this.finish_pos = finish_pos;
        }

        void setStart_location(String start_location) {
            this.start_location = start_location;
        }

        void setFinish_location(String finish_location) {
            this.finish_location = finish_location;
        }

        public void setDistance(int distance) {
            this.distance = distance;
        }

        void setSeats(int seats) {
            this.seats = seats;
        }
    }
}

