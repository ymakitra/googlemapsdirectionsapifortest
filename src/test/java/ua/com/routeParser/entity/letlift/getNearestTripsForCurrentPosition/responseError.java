package ua.com.routeParser.entity.letlift.getNearestTripsForCurrentPosition;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Alex on 02.05.2017.
 */
public class responseError {
    @SerializedName("name")
    String mName;

    @SerializedName("message")
    String mMessage;

    @SerializedName("code")
    String mCode;

    @SerializedName("type")
    String mType;

    @SerializedName("file")
    String mfile;

    @SerializedName("line")
    String mline;

    @SerializedName("stack-trace")
    String mstacktrace;
}
