package ua.com.routeParser.entity.letlift.driverTrip;

import com.google.gson.annotations.SerializedName;


public class createTrip {
    @SerializedName("car_id")
    int mCar_id;

    @SerializedName("start_pos")
    String mStart_pos;

    @SerializedName("finish_pos")
    String mFinish_pos;

    @SerializedName("start_location")
    String mStart_location;

    @SerializedName("finish_location")
    String mFinish_location;

    @SerializedName("distance")
    String mDistance;
}
