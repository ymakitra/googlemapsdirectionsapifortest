package ua.com.routeParser.entity.letlift.getNearestTripsForCurrentPosition;

        import com.google.gson.annotations.SerializedName;


public class requestPosition {
    @SerializedName("start_pos")
    String mStart_pos;

    @SerializedName("finish_pos")
    String mFinish_pos;

    @SerializedName("seats")
    String mSeats;

    @SerializedName("expandParam")
    String mExpandParam;
}
