package ua.com.routeParser.entity.letlift.authorization.logIn.userResponse;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Alex on 19.04.2017.
 */
public class UserResponse {

    @SerializedName("id")
    String mId;

    @SerializedName("email")
    String mEmail;

    @SerializedName("first_name")
    String mFirst_name;

    @SerializedName("last_name")
    String mLast_name;

    @SerializedName("photo")
    String mPhoto;

    @SerializedName("birth_date")
    String mBirth_date;

    @SerializedName("rating")
    String mRating;
}
