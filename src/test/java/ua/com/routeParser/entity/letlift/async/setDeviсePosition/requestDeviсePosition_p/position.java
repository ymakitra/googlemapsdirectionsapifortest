package ua.com.routeParser.entity.letlift.async.setDeviсePosition.requestDeviсePosition_p;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Alex on 25.04.2017.
 */
public class position {
    @SerializedName("lat")
    String mLat;

    @SerializedName("long")
    String mLong;

    @SerializedName("speed")
    String mSpeed;

    @SerializedName("bearing")
    String mBearing;



    public void setmLat(String mLat) {
        this.mLat = mLat;
    }

    public void setmLong(String mLong) {
        this.mLong = mLong;
    }

    public void setmSpeed(String mSpeed) {
        this.mSpeed = mSpeed;
    }

    public void setmBearing(String mBearing) {
        this.mBearing = mBearing;
    }
}
