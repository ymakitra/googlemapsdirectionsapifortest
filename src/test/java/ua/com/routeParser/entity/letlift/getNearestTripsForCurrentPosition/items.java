package ua.com.routeParser.entity.letlift.getNearestTripsForCurrentPosition;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Alex on 02.05.2017.
 */
public class items {
    @SerializedName("id")
    String mId;

    @SerializedName("car_id")
    String mcar_id;

    @SerializedName("user_id")
    String mUser_id;

    @SerializedName("start_pos")
    String mstart_pos;

    @SerializedName("finish_pos")
    String mfinish_pos;

    @SerializedName("start_location")
    String mstart_location;

    @SerializedName("finish_location")
    String mfinish_location;

    @SerializedName("distance")
    String mdistance;

    @SerializedName("seats")
    String mseats;

    @SerializedName("started_at")
    String mstarted_at;

    @SerializedName("finished_at")
    String mfinished_at;
}
