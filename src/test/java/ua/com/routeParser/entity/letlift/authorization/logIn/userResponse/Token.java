package ua.com.routeParser.entity.letlift.authorization.logIn.userResponse;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Alex on 19.04.2017.
 */
public class Token {
    @SerializedName("token")
    String mToken;

    @SerializedName("expired_at")
    String mExpired_at;
}
