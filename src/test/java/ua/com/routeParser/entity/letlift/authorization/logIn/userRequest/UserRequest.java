package ua.com.routeParser.entity.letlift.authorization.logIn.userRequest;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Alex on 19.04.2017.
 */
public class UserRequest {

    @SerializedName("email")
    String mEmail;

    @SerializedName("password")
    String mPassword;

    @SerializedName("first_name")
    String mFirst_name;

    @SerializedName("birth_date")
    String mBirth_date;

    @SerializedName("birth_date")
    String mSex;
}
