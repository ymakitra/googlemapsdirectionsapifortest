package ua.com.routeParser.entity.letlift.async.setDeviсePosition.requestDeviсePosition_p;

import com.google.gson.annotations.SerializedName;


public class requestDevicePosition {
    public requestDevicePosition(){
        this.mPosition = new position();
        this.mTrip = new trip();
    }

    @SerializedName("position")
    position mPosition;

    @SerializedName("trip")
    trip mTrip;

    public position getmPosition() {
        return mPosition;
    }

    public trip getmTrip() {
        return mTrip;
    }


}
