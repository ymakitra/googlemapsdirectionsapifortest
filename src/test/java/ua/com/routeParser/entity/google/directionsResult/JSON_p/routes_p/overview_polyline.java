package ua.com.routeParser.entity.google.directionsResult.JSON_p.routes_p;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Alex on 19.04.2017.
 */
public class overview_polyline {
    @SerializedName("points")
    String mPoints;

    public String getmPoints() {
        return mPoints;
    }
}
