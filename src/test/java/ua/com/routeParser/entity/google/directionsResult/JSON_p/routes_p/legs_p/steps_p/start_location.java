package ua.com.routeParser.entity.google.directionsResult.JSON_p.routes_p.legs_p.steps_p;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Alex on 19.04.2017.
 */
public class start_location {
    @SerializedName("text")
    String lat;

    @SerializedName("value")
    String lng;

    public String getLat() {
        return lat;
    }

    public String getLng() {
        return lng;
    }
}
