package ua.com.routeParser.entity.google.directionsResult.JSON_p.routes_p.legs_p;

import com.google.gson.annotations.SerializedName;
import ua.com.routeParser.entity.google.directionsResult.JSON_p.routes_p.legs_p.steps_p.steps;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex on 19.04.2017.
 */
public class legs {

    @SerializedName("end_address")
    String mEnd_address;

    @SerializedName("start_address")
    String mStart_address;

    @SerializedName("distance")
    distance mDistance;

    @SerializedName("duration")
    duration mDuration;

    @SerializedName("start_location")
    start_location mStart_location;

    @SerializedName("steps")
    List<steps> mSteps = new ArrayList<steps>();

    public String getmEnd_address() {
        return mEnd_address;
    }

    public String getmStart_address() {
        return mStart_address;
    }

    public distance getmDistance() {
        return mDistance;
    }

    public duration getmDuration() {
        return mDuration;
    }

    public start_location getmStart_location() {
        return mStart_location;
    }

    public List<steps> getmSteps() {
        return mSteps;
    }

}
