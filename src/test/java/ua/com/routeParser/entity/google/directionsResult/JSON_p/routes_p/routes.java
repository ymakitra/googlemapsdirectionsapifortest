package ua.com.routeParser.entity.google.directionsResult.JSON_p.routes_p;

import com.google.gson.annotations.SerializedName;
import ua.com.routeParser.entity.google.directionsResult.JSON_p.routes_p.bounds_p.bounds;
import ua.com.routeParser.entity.google.directionsResult.JSON_p.routes_p.legs_p.legs;

import java.util.ArrayList;
import java.util.List;


public class routes {

    @SerializedName("bounds")
    bounds mBounds;

    @SerializedName("copyrights")
    String mCopyrights;

    @SerializedName("legs")
    List<legs> mLegs = new ArrayList<legs>();

    @SerializedName("summary")
    String mSummary;

    @SerializedName("warnings")
    List<warnings> mWarnings = new ArrayList<warnings>();

    @SerializedName("waypoint_order")
    List<waypoint_order> mWaypoint_order = new ArrayList<waypoint_order>();

    @SerializedName("overview_polyline")
    overview_polyline mOverview_polyline;

    public bounds getmBounds() {
        return mBounds;
    }

    public String getmCopyrights() {
        return mCopyrights;
    }

    public List<legs> getmLegs() {
        return mLegs;
    }

    public String getmSummary() {
        return mSummary;
    }

    public List<warnings> getmWarnings() {
        return mWarnings;
    }

    public List<waypoint_order> getmWaypoint_order() {
        return mWaypoint_order;
    }

    public overview_polyline getmOverview_polyline() {
        return mOverview_polyline;
    }
}
