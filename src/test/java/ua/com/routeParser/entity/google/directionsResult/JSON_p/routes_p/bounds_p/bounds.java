package ua.com.routeParser.entity.google.directionsResult.JSON_p.routes_p.bounds_p;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Alex on 19.04.2017.
 */
public class bounds {
    @SerializedName("northeast")
    northeast mNortheast;

    @SerializedName("southwest")
    southwest mSouthwest;

    public northeast getmNortheast() {
        return mNortheast;
    }

    public southwest getmSouthwest() {
        return mSouthwest;
    }
}
