package ua.com.routeParser.entity.google.directionsResult.JSON_p.routes_p.bounds_p;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Alex on 19.04.2017.
 */
public class northeast {
    @SerializedName("lat")
    String mLat;

    @SerializedName("lng")
    String mLng;

    public String getmLat() {
        return mLat;
    }

    public String getmLng() {
        return mLng;
    }
}
