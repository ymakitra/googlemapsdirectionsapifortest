package ua.com.routeParser.entity.google.directionsResult.JSON_p.routes_p.legs_p;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Alex on 19.04.2017.
 */
public class duration {
    @SerializedName("text")
    String text;

    @SerializedName("value")
    String value;

    public String getText() {
        return text;
    }

    public String getValue() {
        return value;
    }
}
