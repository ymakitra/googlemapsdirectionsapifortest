package ua.com.routeParser.entity.google.directionsResult.JSON_p.routes_p.legs_p.steps_p;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex on 19.04.2017.
 */
public class steps {
    @SerializedName("distance")
    distance mDistance;

    @SerializedName("duration")
    duration mDuration;

    @SerializedName("end_location")
    end_location mEnd_location;

    @SerializedName("html_instructions")
    String mHtml_instructions;

    @SerializedName("polyline")
    polyline mPolyline;

    @SerializedName("start_location")
    start_location mStart_location;

    @SerializedName("travel_mode")
    String mTravel_mode;

    @SerializedName("traffic_speed_entry")
    List<traffic_speed_entry> mTraffic_speed_entry = new ArrayList<traffic_speed_entry>();

    @SerializedName("via_waypoint")
    List<via_waypoint> mVia_waypoint = new ArrayList<via_waypoint>();

    public distance getmDistance() {
        return mDistance;
    }

    public duration getmDuration() {
        return mDuration;
    }

    public end_location getmEnd_location() {
        return mEnd_location;
    }

    public String getmHtml_instructions() {
        return mHtml_instructions;
    }

    public polyline getmPolyline() {
        return mPolyline;
    }

    public start_location getmStart_location() {
        return mStart_location;
    }

    public String getmTravel_mode() {
        return mTravel_mode;
    }

    public List<traffic_speed_entry> getmTraffic_speed_entry() {
        return mTraffic_speed_entry;
    }

    public List<via_waypoint> getmVia_waypoint() {
        return mVia_waypoint;
    }
}
