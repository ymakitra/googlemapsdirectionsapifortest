package ua.com.routeParser.entity.google.directionsResult.JSON_p;

import com.google.gson.annotations.SerializedName;
import ua.com.routeParser.entity.google.directionsResult.JSON_p.geocoded_waypoints_p.geocoded_waypoints;
import ua.com.routeParser.entity.google.directionsResult.JSON_p.routes_p.routes;

import java.util.ArrayList;
import java.util.List;


public class JSON {
    @SerializedName("status")
    String mStatus;

    @SerializedName("geocoded_waypoints")
    List<geocoded_waypoints> mGeocoded_waypoints = new ArrayList<geocoded_waypoints>();

    @SerializedName("routes")
    List<routes> mRoutes = new ArrayList<routes>();

    public String getmStatus() {

        return mStatus;
    }

    public List<geocoded_waypoints> getmGeocoded_waypoints() {

        return mGeocoded_waypoints;
    }

    public List<routes> getmRoutes() {

        return mRoutes;
    }
}
