package ua.com.routeParser.service;

import com.google.gson.reflect.TypeToken;
import com.google.maps.model.LatLng;
import ua.com.routeParser.connection.CRequest;
import ua.com.routeParser.connection.CResult;
import ua.com.routeParser.connection.ServerConnection;
import ua.com.routeParser.entity.google.directionsResult.JSON_p.JSON;
import ua.com.routeParser.entity.letlift.async.setDeviсePosition.requestDeviсePosition_p.position;
import ua.com.routeParser.entity.letlift.async.setDeviсePosition.responseDevicePosition_p.responseMessage;
import ua.com.routeParser.entity.letlift.getNearestTripsForCurrentPosition.responsePosition;

import java.util.ArrayList;
import java.util.List;


public class ApiManager {
    String YOUR_API_KEY2 = "AIzaSyD3wF7R_QrkWJTnfUs5WZ64A2GXSkHn-SU";

    //максимальна відстань між координатами які ми довизначаємо
    public static final double MIN_STEP = 0.05;

    public List<LatLng> getGoogleDirections(String startLat, String startLng, String endLat, String endLng){
        JSON json = new JSON();
        GeoDataParser geoDataParser = new GeoDataParser();
        String start = startLat + "%20" + startLng;
        String finish = endLat + "%20" + endLng;
        String mode = "driving";
        CRequest mRequest = new CRequest();
        CResult result = new CResult();

        mRequest.mURL = "https://maps.googleapis.com/maps/api/directions/json?origin=" + start + "&destination=" + finish + "&mode=" + mode + "&key=" + YOUR_API_KEY2;
        ServerConnection serverConnection = new ServerConnection(mRequest, ServerConnection.FMethod.POST, null);
        serverConnection.setResponseType(new TypeToken<JSON>() {}.getType(), false);
        json = (JSON) serverConnection.connect().ObjResult;

        List<LatLng> lstCoord = new ArrayList<LatLng>();

        //отримуємо масив координат із полілінії
        lstCoord = geoDataParser.decodePoly(json.getmRoutes().get(0).getmOverview_polyline().getmPoints());

        //Додаємо нові точки у проміжках для дотримання заданої максимальної відстані
        for(int i = 0; i < lstCoord.size() - 1; i++){
            while (geoDataParser.distance(lstCoord.get(i).lat, lstCoord.get(i).lng, lstCoord.get(i + 1).lat, lstCoord.get(i + 1).lng,"K") > MIN_STEP) {
                lstCoord.add(i + 1, geoDataParser.midPoint(lstCoord.get(i).lat, lstCoord.get(i).lng, lstCoord.get(i + 1).lat, lstCoord.get(i + 1).lng));
                i = 0;
            }
        }
//        System.out.println(lstCoord);
        return lstCoord;
    }
    public CResult getNearestTripsForCurrentPosition (String start_pos, String finish_pos, String seats, String expandParam, String token){
        CRequest mRequest = new CRequest();
        CResult result = new CResult();
//        LoginUser loginUser = new LoginUser();

        mRequest.mURL = "https://api.letlift.demo.relevant.software/v1/driver-trips/intersect?start_pos=" + start_pos + "&finish_pos="
                + finish_pos + "&seats=" + seats + "&&expand=" + expandParam;

        ServerConnection serverConnection = new ServerConnection(mRequest, ServerConnection.FMethod.GET, token);
        serverConnection.setResponseType(new TypeToken<responsePosition>() {}.getType(), false);
        return serverConnection.connect();
    }

    public CResult setDevicePosition (String id, String lat, String lon , String speed , String bearing){
        CRequest mRequest = new CRequest();
        CResult result = new CResult();
        mRequest.mURL = "https://api.letlift.demo.relevant.software/v1/async/position/set/" + id;
        position mPosition = new position();
        mPosition.setmLat(lat);
        mPosition.setmLong(lon);
        mPosition.setmSpeed(speed);
        mPosition.setmBearing(bearing);
        mRequest.ObjRequest = mPosition;
        ServerConnection serverConnection = new ServerConnection(mRequest,ServerConnection.FMethod.POST, null);
        serverConnection.setResponseType(new TypeToken<responseMessage>() {}.getType(), false);
        return serverConnection.connect();

    }

}
