package ua.com.routeParser.service;


import com.google.gson.Gson;
import com.google.maps.model.LatLng;
import ua.com.authorization.Authorization;
import ua.com.routeParser.connection.CResult;
import ua.com.Variables;
import ua.com.VariablesMulti;

import java.util.ArrayList;
import java.util.List;


public class Main {
    // public static final double MIN_STEP = 0.05;


    public static void main(String[] arg) {
        ApiManager apiManager = new ApiManager();
        List<LatLng> latLng1 = new ArrayList<LatLng>();
        latLng1 = apiManager.getGoogleDirections("49.8445500", "23.9970000", "49.842754", "24.009286");
        List<LatLng> latLng2 = new ArrayList<LatLng>();
        latLng2 = apiManager.getGoogleDirections("49.844597", "23.996712", "49.839599", "23.995253");

        VariablesMulti variablesMulti = new VariablesMulti();
        String start_pos = variablesMulti.getStartLatList().get(0) + "," + variablesMulti.getStartLngList().get(0);
        String finish_pos = variablesMulti.getEndLatList().get(0) + "," + variablesMulti.getEndLngList().get(0);

        CResult result = apiManager.getNearestTripsForCurrentPosition(start_pos, finish_pos,"1","", logIn());

        toConsole(result);

    }


    public static String logIn(){
        //LOGIN
        Variables variables = new Variables();
        String email = variables.getEmail();
        String password = variables.getPassword();

        String loginToken = Authorization.Login(email, password);
        return loginToken;
    }
    public static void toConsole(CResult result) {

        Gson gson = new Gson();
        String request = gson.toJson(result.ObjResult);
        System.out.println(request);

    }
}



