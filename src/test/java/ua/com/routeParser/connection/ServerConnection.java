package ua.com.routeParser.connection;

import com.google.gson.Gson;
import org.json.JSONObject;
import ua.com.routeParser.entity.google.directionsResult.JSON_p.JSON;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.*;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;


public class ServerConnection {
    String mURL;
    CRequest mCRequest;
    CResult mCResult;
    FMethod mMethod;
    Type mResponseType;
    boolean mResponseTypeIsList;
    String mToken = null;

    public ServerConnection(CRequest request, FMethod method, String token){
        this.mCRequest = request;
        this.mMethod = method;
        this.mToken = token;
        this.mURL = mCRequest.mURL;
    }

    public void setResponseType(Type responseType, boolean responseTypeIsList) {
        this.mResponseType = responseType;
        this.mResponseTypeIsList = responseTypeIsList;

    }

    public enum FMethod {
        GET,
        POST
    }
    public CResult connect(){
        String ServerResponse = "";
        JSON json = new JSON();
//        boolean result = false;
        CResult mCResult = new CResult();
        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts = new TrustManager[]{
                new X509TrustManager() {
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }
                    public void checkClientTrusted(
                            java.security.cert.X509Certificate[] certs, String authType) {
                    }
                    public void checkServerTrusted(
                            java.security.cert.X509Certificate[] certs, String authType) {
                    }
                }
        };

        // Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
        }
        switch (mMethod) {
            case POST:
                try {
                    URL url = new URL(mURL);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    if (mToken != null){
                        connection.setRequestProperty("Authorization","Bearer " + mToken);
                    }
                    if(mCRequest.ObjRequest != null){
                        connection.setDoOutput(true);
                        connection.setDoInput(true);
                        connection.setRequestMethod("POST");
                        connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                        Gson gson = new Gson();
                        String request = gson.toJson(mCRequest.ObjRequest);
                        JSONObject reqJson = new JSONObject(request);
                        OutputStream out = connection.getOutputStream();
                        BufferedWriter writer = new BufferedWriter(
                                new OutputStreamWriter(out, "UTF-8"));
                        writer.write(reqJson.toString());
                        writer.flush();
                        writer.close();
                        out.close();
                    }
                    //Get Input/ вхідний потік помилок
                    InputStream isEror = connection.getErrorStream();
                    if (isEror == null) {
                        InputStream is = connection.getInputStream();
                        BufferedReader rd = new BufferedReader(new InputStreamReader(is));

                        JSON json_res = new JSON();
                        //json_res =  new Gson().fromJson(rd, mResponseType);
                       if (mResponseTypeIsList){
                           mCResult.ObjListResult = new Gson().fromJson(rd, mResponseType);
                       } else {
                           mCResult.ObjResult = new Gson().fromJson(rd, mResponseType);
                       }

                        rd.close();
                        mCResult.SERVER_RESPONSE = "true";
                    } else {
                        mCResult.SERVER_RESPONSE = "false";
                    }
                } catch (MalformedURLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case GET:
                try {
                    URL url = new URL(mURL);
                    HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
                    if (mToken != null){
                        connection.setRequestProperty("Authorization","Bearer " + mToken);
                    }

                    connection.setDoOutput(false);
                    connection.setDoInput(true);
                    connection.setRequestMethod("GET");
                    connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");


                    //Get Input/ вхідний потік помилок
                    InputStream isEror = connection.getErrorStream();
                    if (isEror == null) {
                        InputStream is = connection.getInputStream();
                        BufferedReader rd = new BufferedReader(new InputStreamReader(is));

                        JSON json_res = new JSON();
                        if (mResponseTypeIsList){
                            mCResult.ObjListResult = new Gson().fromJson(rd, mResponseType);
                        } else {
                            mCResult.ObjResult = new Gson().fromJson(rd, mResponseType);
                        }

                        rd.close();
                        mCResult.SERVER_RESPONSE = "true";
                    } else {
                        mCResult.SERVER_RESPONSE = "false";
                    }
                } catch (MalformedURLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
        return mCResult;
    }
}
