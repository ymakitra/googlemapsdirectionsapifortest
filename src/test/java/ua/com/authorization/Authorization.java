package ua.com.authorization;

import io.restassured.response.Response;
import ua.com.Url;
import ua.com.authorization.newUserResp.RegisterANewUserResp;
import java.text.SimpleDateFormat;
import java.util.Date;
import static io.restassured.RestAssured.given;

public class Authorization {

    public static String Login(String email, String password) {
        String url = Url.base;
        String urlLogin = Url.login;

        Response post;
        post = given().
                relaxedHTTPSValidation().
                param("email", email).
                param("password", password).
                expect().
                statusCode(200).
//                log().all().
        when().
                        post(url + urlLogin).
                        then().
                        extract().
                        response();

//        GET TOKEN FROM RESPONSE
//        String loginToken = loginPost.path("data.token.token");
//        System.out.println("LoginToken: " + loginToken);
        return post.path("data.token.token");
    }

    public static String Logout(String loginToken) {
        String url = Url.base;
        String urlLogout = Url.logout;

        Response post;
        post = given().
                relaxedHTTPSValidation().
                header("Authorization", ("Bearer " + loginToken)).
                expect().
                statusCode(200).
//                log().all().
        when().
                        post(url + urlLogout).
                        then().
                        extract().
                        response();
        return post.path("data.message");
    }

    public static RegisterANewUserResp Register() {
        String url = Url.base;
        String urlRegister = Url.register;
        String out = new SimpleDateFormat("yyyyMMdd.hhmmss").format(new Date());

        Response post;
        post = given().
                relaxedHTTPSValidation().
                param("email", ("KillMe" + out + "@xxx.xxx")).
                param("password", "12345678").
                param("first_name", "RegisterTest").
                param("last_name", "YouCanDeleteMe").
                param("birth_date", 564364800).
                param("sex", "female").
                expect().
                statusCode(200).
//                log().all().
                when().
                post(url + urlRegister).
                then().
                extract().
                response();
        RegisterANewUserResp dataFromResp = post.as(RegisterANewUserResp.class);
        return dataFromResp;
    }

    public static String ChangePassword(String loginToken, String password, String passwordRepeat) {
        String url = Url.base;
        String urlChangePassword = Url.changePassword;

        Response post;
        post = given().
                relaxedHTTPSValidation().
                param("token", loginToken).
                param("password", password).
                param("password_repeat", passwordRepeat).
                expect().
                statusCode(200).
//                log().all().
        when().
                        post(url + urlChangePassword).
                        then().
                        extract().
                        response();
        return post.path("data.message");
    }

    public static String ResetPassword(String email) {
        String url = Url.base;
        String urlResetPassword = Url.resetPassword;

        Response post;
        post = given().
                relaxedHTTPSValidation().
                param("email", email).
                expect().
                statusCode(200).
//                log().all().
        when().
                        post(url + urlResetPassword).
                        then().
                        extract().
                        response();
        return post.path("data.message");
    }
}
