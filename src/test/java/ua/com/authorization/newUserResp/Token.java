
package ua.com.authorization.newUserResp;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Token implements Serializable
{

    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("expired_at")
    @Expose
    private Integer expiredAt;
    private final static long serialVersionUID = -6891155104437629351L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Token() {
    }

    /**
     * 
     * @param token
     * @param expiredAt
     */
    public Token(String token, Integer expiredAt) {
        super();
        this.token = token;
        this.expiredAt = expiredAt;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Integer getExpiredAt() {
        return expiredAt;
    }

    public void setExpiredAt(Integer expiredAt) {
        this.expiredAt = expiredAt;
    }

}
