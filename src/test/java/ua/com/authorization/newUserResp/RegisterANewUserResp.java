
package ua.com.authorization.newUserResp;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RegisterANewUserResp implements Serializable
{

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("data")
    @Expose
    private Data data;
    private final static long serialVersionUID = 1421289396160578861L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public RegisterANewUserResp() {
    }

    /**
     * 
     * @param data
     * @param success
     */
    public RegisterANewUserResp(Boolean success, Data data) {
        super();
        this.success = success;
        this.data = data;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

}
