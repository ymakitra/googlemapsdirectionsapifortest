
package ua.com.authorization.newUserResp;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User implements Serializable
{

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("organization_id")
    @Expose
    private Object organizationId;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("photo")
    @Expose
    private Object photo;
    @SerializedName("tmp_photo")
    @Expose
    private Object tmpPhoto;
    @SerializedName("sex")
    @Expose
    private String sex;
    @SerializedName("birth_date")
    @Expose
    private String birthDate;
    @SerializedName("rating")
    @Expose
    private Object rating;
    @SerializedName("female_only")
    @Expose
    private Integer femaleOnly;
    private final static long serialVersionUID = 966819074625595282L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public User() {
    }

    /**
     * 
     * @param tmpPhoto
     * @param id
     * @param lastName
     * @param sex
     * @param femaleOnly
     * @param email
     * @param birthDate
     * @param rating
     * @param organizationId
     * @param firstName
     * @param photo
     */
    public User(Integer id, Object organizationId, String email, String firstName, String lastName, Object photo, Object tmpPhoto, String sex, String birthDate, Object rating, Integer femaleOnly) {
        super();
        this.id = id;
        this.organizationId = organizationId;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.photo = photo;
        this.tmpPhoto = tmpPhoto;
        this.sex = sex;
        this.birthDate = birthDate;
        this.rating = rating;
        this.femaleOnly = femaleOnly;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Object getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Object organizationId) {
        this.organizationId = organizationId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Object getPhoto() {
        return photo;
    }

    public void setPhoto(Object photo) {
        this.photo = photo;
    }

    public Object getTmpPhoto() {
        return tmpPhoto;
    }

    public void setTmpPhoto(Object tmpPhoto) {
        this.tmpPhoto = tmpPhoto;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public Object getRating() {
        return rating;
    }

    public void setRating(Object rating) {
        this.rating = rating;
    }

    public Integer getFemaleOnly() {
        return femaleOnly;
    }

    public void setFemaleOnly(Integer femaleOnly) {
        this.femaleOnly = femaleOnly;
    }

}
