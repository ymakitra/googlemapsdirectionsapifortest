package ua.com;

import com.google.maps.model.LatLng;
import ua.com.async.SetDevicePosition;
import ua.com.authorization.Authorization;
import ua.com.driverTrip.DriverTrip;
import ua.com.routeParser.service.ApiManager;
import ua.com.service.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;


public class MainMulti {
    public static void main(String[] args) throws IOException, InterruptedException, NullPointerException {

//        LOGIN (get TOKEN)
        VariablesMulti variablesMulti = new VariablesMulti();
        Variables variables = new Variables();
        String driverDevToken = variables.getDeviceTokenOne();
        int numberOfDrivers = variablesMulti.emailList.size();
        System.out.println("Number of drivers: " + numberOfDrivers);

        ArrayList<Integer> tripIdList = new ArrayList<Integer>();
        ArrayList<String> loginTokenList = new ArrayList<String>();

        for (int i = 0; i < numberOfDrivers; i++) {

            String loginToken = Authorization.Login(variablesMulti.emailList.get(i), variablesMulti.getPassword());
            loginTokenList.add(loginToken);

//        CRETE TRIP (get TRIP ID)
            int tripId = DriverTrip.CreateTrip(variablesMulti.startLatList.get(i), variablesMulti.startLngList.get(i),
                    variablesMulti.endLatList.get(i), variablesMulti.endLngList.get(i), loginTokenList.get(i), variablesMulti.getCarIdList().get(i), driverDevToken);
            tripIdList.add(tripId);
        }
        System.out.println("loginTokenList: " + loginTokenList);
        System.out.println("tripIdList: " + tripIdList);

//        GET POINTS (get POINTS OF TRIP)
        AllUsers allUsers = new AllUsers();
        List<Integer> pointsListSize = new ArrayList<Integer>();

        for (int i = 0; i < numberOfDrivers; i++) {
            ApiManager apiManager = new ApiManager();
            List<LatLng> tripPointsList = apiManager.getGoogleDirections(variablesMulti.startLatList.get(i),
                    variablesMulti.startLngList.get(i), variablesMulti.endLatList.get(i), variablesMulti.endLngList.get(i));
            pointsListSize.add(tripPointsList.size());

            Users users = new Users(tripPointsList);
            allUsers.addUser(users);

            System.out.println("List " + i + ": " + users.toString());
        }
        System.out.println("Size of Arrays: " + pointsListSize);

//        GET MAX LIST SIZE FOR LOOP
        MaxValue getMaxValueFromList = new MaxValue();
        int maxSizeOfList = getMaxValueFromList.GetMaxListSize((ArrayList) pointsListSize);
        System.out.println("Max Size of Array: " + maxSizeOfList);
        System.out.println("==============================================================================");

//        SENT POINTS
        for (int j = 0; j < maxSizeOfList; j++) {
            for (int i = 0; i < numberOfDrivers; i++) {
                try {
                    allUsers.getUser(i).getTripPoint(j);
                    double lat = allUsers.getUser(i).getTripPoint(j).lat;
                    double lng = allUsers.getUser(i).getTripPoint(j).lng;
//                    System.out.println(lat);
//                    System.out.println(lng);
                    new SetDevicePosition(loginTokenList.get(i), Integer.toString(tripIdList.get(i)), Double.toString(lat), Double.toString(lng));

//                    TIMER
                    RandomGenerator randomGenerator = new RandomGenerator();
                    double randomTime = randomGenerator.DoubleRandomGenerator(variablesMulti.getTimeRangeMin(), variablesMulti.getTimeRangeMax());
                    TimeUnit.SECONDS.sleep((long) randomTime);

//                    System.out.println("Timer: " + randomTime);

                } catch (NullPointerException lat) {
                    System.out.println("Point Null");
                }
                System.out.println("Trip number - " + i + "   Point number - " + j);
                System.out.println("------------------------ ");
            }
            System.out.println("=============================================== " + j + "/" + (maxSizeOfList - 1));
        }

//        DELETE TRIP
        Loop.FinishTrip(numberOfDrivers, loginTokenList, tripIdList);
        }
    }

