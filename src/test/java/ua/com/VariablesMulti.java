package ua.com;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class VariablesMulti {
//            V2 - Small set
//    public List<String> emailList = new ArrayList<String>(Arrays.asList("y.makitra1@relevant.software", "y.makitra2@relevant.software", "y.makitra3@relevant.software"));

    //      V3 - 10 routes
    public List<String> emailList = new ArrayList<String>(Arrays.asList("y.makitra1@relevant.software", "y.makitra2@relevant.software", "y.makitra3@relevant.software", "y.makitra4@relevant.software",
            "y.makitra5@relevant.software", "y.makitra6@relevant.software", "y.makitra7@relevant.software", "y.makitra8@relevant.software", "y.makitra9@relevant.software", "y.makitra10@relevant.software"));
    //      V3 - 10 routes                                                    0            1             2           3             4            5             6            7            8           9            10
    public List<String> startLatList = new ArrayList<String>(Arrays.asList("49.8445500", "49.844597", "49.844601", "49.8463970", "49.8473937", "49.839210", "49.845518", "49.838874", "49.843299", "49.841313"));
    public List<String> startLngList = new ArrayList<String>(Arrays.asList("23.9970000", "23.996712", "23.996721", "23.9962090", "24.0015970", "24.000390", "23.994315", "23.999939", "24.010455", "23.999918"));
    public List<String> endLatList = new ArrayList<String>(Arrays.asList("49.842754", "49.839599", "49.838639", "49.842391", "49.8446160", "49.846846", "49.842211", "49.850728", "49.842566", "49.844849"));
    public List<String> endLngList = new ArrayList<String>(Arrays.asList("24.009286", "23.995253", "24.012726", "24.009086", "23.9967100", "24.004272", "24.017220", "23.992443", "24.001067", "24.006102"));

////          V4 - one route for all(20)
//    public List<String> emailList = new ArrayList<String>(Arrays.asList("y.makitra1@relevant.software", "y.makitra2@relevant.software", "y.makitra3@relevant.software", "y.makitra4@relevant.software",
//            "y.makitra5@relevant.software", "y.makitra6@relevant.software", "y.makitra7@relevant.software", "y.makitra8@relevant.software", "y.makitra9@relevant.software", "y.makitra10@relevant.software",
//            "y.makitra20@relevant.software","y.makitra21@relevant.software", "y.makitra22@relevant.software","y.makitra23@relevant.software","y.makitra24@relevant.software", "y.makitra25@relevant.software",
//            "y.makitra26@relevant.software", "y.makitra27@relevant.software", "y.makitra28@relevant.software", "y.makitra29@relevant.software"));
//
////          V4 - one route for all(20)
//    public List<String> startLatList = new ArrayList<String>(Arrays.asList("49.8463970", "49.8463970", "49.8463970", "49.8463970", "49.8463970", "49.8463970", "49.8463970", "49.8463970", "49.8463970", "49.8463970", "49.8463970", "49.8463970", "49.8463970", "49.8463970", "49.8463970", "49.8463970", "49.8463970", "49.8463970", "49.8463970", "49.8463970"));
//    public List<String> startLngList = new ArrayList<String>(Arrays.asList("23.9962090", "23.9962090", "23.9962090", "23.9962090", "23.9962090", "23.9962090", "23.9962090", "23.9962090", "23.9962090", "23.9962090", "23.9962090", "23.9962090", "23.9962090", "23.9962090", "23.9962090", "23.9962090", "23.9962090", "23.9962090", "23.9962090", "23.9962090"));
//    public List<String> endLatList = new ArrayList<String>(Arrays.asList("49.842391", "49.842391", "49.842391", "49.842391", "49.842391", "49.842391", "49.842391", "49.842391", "49.842391", "49.842391", "49.842391", "49.842391", "49.842391", "49.842391", "49.842391", "49.842391", "49.842391", "49.842391", "49.842391", "49.842391"));
//    public List<String> endLngList = new ArrayList<String>(Arrays.asList("24.009086", "24.009086", "24.009086", "24.009086", "24.009086", "24.009086", "24.009086", "24.009086", "24.009086", "24.009086", "24.009086", "24.009086", "24.009086", "24.009086", "24.009086", "24.009086", "24.009086", "24.009086", "24.009086", "24.009086"));



    public List<Integer> carIdList = new ArrayList<Integer>(Arrays.<Integer>asList(9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 237, 238, 239, 240, 241, 242, 243, 244, 245, 246));
    public List<String> driverDevToken = new ArrayList<String>(Arrays.asList("111", "222", "333", "444", "555", "666", "777", "888", "999", "100", "1111", "1222", "1333", "1444", "1555", "1666", "1777", "1888", "1999", "1100"));
    double timeRangeMin = 0.10;
    double timeRangeMax = 1.0;
    int numberOfDrivers = emailList.size();
    String password = "12345678";
    public static final int threadStepMin = 500;
    public static final int threadStepMax = 3000;

    public double getTimeRangeMin() {
        return timeRangeMin;
    }

    public double getTimeRangeMax() {
        return timeRangeMax;
    }

    public List<String> getEmailList() {
        return emailList;
    }

    public String getPassword() {
        return password;
    }

    public List<Integer> getCarIdList() {
        return carIdList;
    }

    public List<String> getStartLatList() {
        return startLatList;
    }

    public List<String> getStartLngList() {
        return startLngList;
    }

    public List<String> getEndLatList() {
        return endLatList;
    }

    public List<String> getEndLngList() {
        return endLngList;
    }
}

