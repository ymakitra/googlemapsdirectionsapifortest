package ua.com;


public class Variables {

    private static String email = "y.makitra@relevant.software";
    private static String password = "12345678";
    protected int carId = 5;
    private float speedMin = 0;
    private float speedMax = 60;
    public double timeMin = 6.5;
    public double timeMax = 10.0;
    protected int distanceMin = 100;
    protected int distanceMax = 5000;
    private String deviceTokenOne = "123456";
    private String deviceTokenTwo = "654321";
    private String deviceTokenThree = "765432";

    String startLat = "49.8445500";
    String startLng = "23.9970000";
    String endLat = "49.842754";
    String endLng = "24.009286";


    public Variables() {

    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public int getCarId() {
        return carId;
    }

    public String getStartLat() {
        return startLat;
    }

    public String getStartLng() {
        return startLng;
    }

    public String getEndLat() {
        return endLat;
    }

    public String getEndLng() {
        return endLng;
    }

    public double getTimeMin() {
        return timeMin;
    }

    public double getTimeRangeMax() {
        return timeMax;
    }

    public float getSpeedMin() {
        return speedMin;
    }

    public float getSpeedRangeMax() {
        return speedMax;
    }

    public int getDistanceMin() {
        return distanceMin;
    }

    public int getDistanceMax() {
        return distanceMax;
    }

    public String getDeviceTokenOne() {
        return deviceTokenOne;
    }

    public String getDeviceTokenTwo() {
        return deviceTokenTwo;
    }

    public String getDeviceTokenThree() {
        return deviceTokenThree;
    }
}
