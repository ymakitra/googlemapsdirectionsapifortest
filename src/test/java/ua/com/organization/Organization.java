package ua.com.organization;

import io.restassured.response.Response;
import ua.com.Url;

import static io.restassured.RestAssured.given;

public class Organization {

    public static int OrganizationsList(String loginToken, String deviceToken, String sort, String expandParams) {
        String url = Url.base;
        String urlTwo = Url.organizationList;

        Response post = given().
                relaxedHTTPSValidation().
                header("Authorization", ("Bearer " + loginToken)).
                header("X-Device-Token", deviceToken).
                param("sort", sort).
                param("expandParams", expandParams).
                expect().
                statusCode(200).
//                log().all().
                when().
                get(url + urlTwo + "?sort=" + sort + "&expand=" + expandParams).

                then().
                extract().
                response();
        return post.path("data.items[0].id");
    }

    public static String GetOrganization(String loginToken, String deviceToken, int orgId, String expandParams) {
        String url = Url.base;
        String urlTwo = Url.organizationList;

        Response post = given().
                relaxedHTTPSValidation().
                header("Authorization", ("Bearer " + loginToken)).
                header("X-Device-Token", deviceToken).
                param("expandParams", expandParams).
                expect().
                statusCode(200).
//                log().all().
                when().
                get(url + urlTwo + "/" + orgId + "?expand=" + expandParams).

                then().
                extract().
                response();
        return post.path("data.name");
    }
}
