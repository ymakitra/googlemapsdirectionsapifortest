package ua.com.service;


import com.google.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

public class Users {
    private List<LatLng> tripList = new ArrayList<>();

    public Users(List<LatLng> tripList) {
        this.tripList = tripList;
    }

//    public List<LatLng> getTripList() {
//        return tripList;
//    }

    public LatLng getTripPoint(int i) {
        if (i < tripList.size()) {
            return (tripList.get(i));
        } else {
            return null;
        }
    }

    @Override
    public String toString() {
        return "User{" +
                "tripList=" + tripList +
                '}';
    }
}



