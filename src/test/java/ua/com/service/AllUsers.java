package ua.com.service;

import java.util.ArrayList;

public  class AllUsers {

    private ArrayList<Users> allUserss = new ArrayList<Users>();

    public int getAllUsersListSize() {
        return allUserss.size();
    }

    public AllUsers() {
//        AllUsersList = allUsersList;
    }

    public void addUser(Users users) {
        allUserss.add(users);
    }

    public ArrayList<Users> getAllUsersList() {
        return allUserss;
    }

    public Users getUser(int i) {
        if (i < allUserss.size()) {
            return (allUserss.get(i));
        } else {
            return null;
        }
    }

    @Override
    public String toString() {
        return "AllUsers{" +
                "allUsers=" + allUserss +
                '}';
    }
}