package ua.com.service;

import com.google.maps.model.LatLng;
import ua.com.VariablesMulti;
import ua.com.async.SetDevicePosition;
import ua.com.authorization.Authorization;
import ua.com.driverTrip.DriverTrip;
import ua.com.routeParser.service.ApiManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static java.lang.Thread.sleep;

public class Loop {

    public static ArrayList<String> TokenList(int numberOfDrivers) {
        ArrayList<String> loginTokenList = new ArrayList<>();
        VariablesMulti variablesMulti = new VariablesMulti();
        for (int i = 0; i < numberOfDrivers; i++) {
//            LoginUser loginUser = new LoginUser();
            String loginToken = Authorization.Login(variablesMulti.emailList.get(i), variablesMulti.getPassword());
            loginTokenList.add(loginToken);
        }
        return loginTokenList;
    }

    public static ArrayList<Integer> TripId(int numberOfDrivers, ArrayList<String> loginTokenList) {
        ArrayList<String> loginTokenLists;
        loginTokenLists = loginTokenList;
        VariablesMulti variablesMulti = new VariablesMulti();
        ArrayList<Integer> tripIdList = new ArrayList<>();
        for (int i = 0; i < numberOfDrivers; i++) {
            int tripId = DriverTrip.CreateTrip(variablesMulti.startLatList.get(i), variablesMulti.startLngList.get(i),
                    variablesMulti.endLatList.get(i), variablesMulti.endLngList.get(i), loginTokenLists.get(i), variablesMulti.getCarIdList().get(i), variablesMulti.driverDevToken.get(i));
            tripIdList.add(tripId);
        }
        return tripIdList;
    }

    public static List<LatLng> GetPoints(int i) {
//        GET POINTS (get POINTS OF TRIP)
        VariablesMulti variablesMulti = new VariablesMulti();
        ApiManager apiManager = new ApiManager();
        return apiManager.getGoogleDirections(variablesMulti.startLatList.get(i), variablesMulti.startLngList.get(i), variablesMulti.endLatList.get(i), variablesMulti.endLngList.get(i));
    }

    public static void SentDriverPosition(int i, int j, AllUsers allUsersTrip, ArrayList<String> loginTokenList, ArrayList<Integer> tripIdList) {

        allUsersTrip.getUser(i).getTripPoint(j);
        double lat = allUsersTrip.getUser(i).getTripPoint(j).lat;
        double lng = allUsersTrip.getUser(i).getTripPoint(j).lng;
//                    System.out.println(lat);
//                    System.out.println(lng);
        new SetDevicePosition(loginTokenList.get(i), Integer.toString(tripIdList.get(i)), Double.toString(lat), Double.toString(lng));
    }



    public static void FinishTrip(int numberOfDrivers, ArrayList<String> loginTokenList, ArrayList<Integer> tripIdList) {
        VariablesMulti variablesMulti = new VariablesMulti();
        for (int i = 0; i < numberOfDrivers; i++) {
            DriverTrip.FinishDriverTrip(loginTokenList.get(i), variablesMulti.driverDevToken.get(i), tripIdList.get(i));
        }
    }
}