package ua.com.service;

import ua.com.VariablesMulti;
import ua.com.async.SetDevicePosition;
import ua.com.routeParser.service.ApiManager;
import java.util.ArrayList;
import java.util.Random;

import static java.lang.Thread.currentThread;
import static java.lang.Thread.sleep;

public class SentDriverPositionThread implements Runnable {
    ApiManager apiManager = new ApiManager();
    int i;
    int maxListSize;
    AllUsers allUsersTrip;
    ArrayList<String> loginTokenList;
    ArrayList<Integer> tripIdList;


    public SentDriverPositionThread(int i, int maxListSize, AllUsers allUsersTrip, ArrayList<String> loginTokenList, ArrayList<Integer> tripIdList) {
        this.i = i;
        this.maxListSize = maxListSize;
        this.allUsersTrip = allUsersTrip;
        this.loginTokenList = loginTokenList;
        this.tripIdList = tripIdList;
    }

    @Override
    public void run() {
        for (int j = 0; j < maxListSize; j++) {
            Random random = new Random();
            try {
                sleep(RandomGenerator.IntRandomGenerator(VariablesMulti.threadStepMin, VariablesMulti.threadStepMax));
                allUsersTrip.getUser(i).getTripPoint(j);
                double lat = allUsersTrip.getUser(i).getTripPoint(j).lat;
                double lng = allUsersTrip.getUser(i).getTripPoint(j).lng;
//                    System.out.println(lat + " " + lng );
//                    System.out.println(lng);
                new SetDevicePosition(loginTokenList.get(i), Integer.toString(tripIdList.get(i)), Double.toString(lat), Double.toString(lng));
                System.out.println("Trip number - " + i + "   Point number - " + j);
                System.out.println("------------------------ ");

            } catch (NullPointerException lat) {
                System.out.println("=============================================== " + i + " - trip end");
                currentThread().stop();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
//            System.out.println("Trip number - " + (i + 1) + "   Point number - " + j);
//            System.out.println("------------------------ ");
        }
//        System.out.println("=============================================== " + i + " - trip end");
    }
}

