package ua.com.service;

import java.util.Random;

public class RandomGenerator {

    public static float floatRandomGenerator(float min, float max) {
        Random r = new Random();
        return min + r.nextFloat() * (max - min);
    }

    public static int IntRandomGenerator (int min, int max) {
        Random r = new Random();
        return r.nextInt(max - min + 1) + min;
    }

    public static double DoubleRandomGenerator (double min, double max) {
        Random r = new Random();
        double random = min + (max - min) * r.nextDouble();
        System.out.println("Timer: " + random);
        return random;
    }

    public RandomGenerator() {
    }


}
