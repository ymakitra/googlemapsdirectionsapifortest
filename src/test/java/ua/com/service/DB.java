package ua.com.service;

import java.sql.*;
import java.util.Properties;

public class DB {
    public static String ResetToken() throws SQLException {

        // create three connections to three different databases on localhost
        Statement stmt;
        Connection conn3;
        String result = null;

        // Connect method #3
        String dbURL = "jdbc:postgresql://letlift.demo.relevant.software:5432/letliftdb";
        Properties parameters = new Properties();
        parameters.put("user", "letlift");
        parameters.put("password", "6xc3wirZ");

        conn3 = DriverManager.getConnection(dbURL, parameters);
        if (conn3 != null) {
            System.out.println("Connected to database");
            stmt = conn3.createStatement();
            String sql = "SELECT token FROM user_password_reset WHERE user_id = 13";
            if (stmt.execute(sql)) {
                //ResultSet Available
                ResultSet rs = stmt.getResultSet();
                rs.next();
                result = rs.getString(1);
            } else {
                //Update count or no result available
                result = "Opsss!";
            }
//            System.out.println(result);
            stmt.close();
        }
        return result;
    }
}
