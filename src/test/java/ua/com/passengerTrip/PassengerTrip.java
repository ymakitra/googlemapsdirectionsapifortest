package ua.com.passengerTrip;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import ua.com.Url;
import ua.com.Variables;
import ua.com.passengerTrip.GSON.updatePassangerTripResp.UpdatePassengerTripResp;
import ua.com.service.RandomGenerator;
import ua.com.staticScenarios.StaticVariables;
import static io.restassured.RestAssured.given;

public class PassengerTrip {
    public static int AllPassengerTrips(String loginToken, String deviceToken, String attribute, String attributeValue, String sort, String expandParams) {
        String url = Url.base;
        String passengerTrip = Url.passengerTrip;

        Response Post;
        Post = given().
                relaxedHTTPSValidation().
                header("Authorization", ("Bearer " + loginToken)).
                header("X-Device-Token", deviceToken).
                param("attribute", attribute).
                param("attributeValue", attributeValue).
                param("sort", sort).
                param("expandParams", expandParams).
                expect().
                statusCode(200).
//                log().all().
        when().
                        get(url + passengerTrip + "?:attribute=" + attributeValue + "&sort=" + sort + "&expand=" + expandParams).
                        then().
                        extract().
                        response();

//        GET TRIP ID FROM RESPONSE
//        System.out.println(enquiryId);
        return Post.path("data.items[0].user_id");
    }


    public static int CreatePassengerTrip(String loginToken, String deviceToken, int driverTripId, String startLat, String startLng, String endLat, String endLng) {
        String url = Url.base;
        String passengerTrips = Url.passengerTrip;

        Variables variables = new Variables();
        int distance = RandomGenerator.IntRandomGenerator(variables.getDistanceMin(), variables.getDistanceMax());
        String distanceS = Integer.toString(distance);

        Response Post;
        Post = given().
                relaxedHTTPSValidation().
                header("Authorization", ("Bearer " + loginToken)).
                header("X-Device-Token", deviceToken).
                param("driver_trip_id", driverTripId).
                param("start_pos", (startLat + ", " + startLng)).
                param("finish_pos", (endLat + ", " + endLng)).
                param("start_location", "Pink Str").
                param("finish_location", "Blue Str").
                param("distance", distanceS).
                expect().
                statusCode(201).
//                log().all().
        when().
                        post(url + passengerTrips).
                        then().
                        extract().
                        response();

//        GET TRIP ID FROM RESPONSE
//        System.out.println(enquiryId);
        return Post.path("data.id");
    }

    public static String CancelPassengerTrip(String loginToken, String deviceToken, int tripId) {
        String url = Url.base;
        String passengerTrips = Url.passengerTrips;
        String cancel = Url.cancel;

        Response Post;
        Post = given().
                relaxedHTTPSValidation().
                header("Authorization", ("Bearer " + loginToken)).
                header("X-Device-Token", deviceToken).
                param("id", tripId).
                expect().
                statusCode(200).
//                log().all().
        when().
                        post(url + passengerTrips + tripId + cancel).
                        then().
                        extract().
                        response();

//        GET TRIP ID FROM RESPONSE
//        System.out.println(enquiryId);
        return Post.path("data.message");
    }

    public static int GetActiveTripForPassengerSuccess(String loginToken, String deviceToken) {
        String url = Url.base;
        String activeTrip = Url.passengerActiveTrip;

        Response Post;
        Post = given().
                relaxedHTTPSValidation().
                header("Authorization", ("Bearer " + loginToken)).
                header("X-Device-Token", deviceToken).
                expect().
                statusCode(200).
//                log().all().
                when().
                get(url + activeTrip).
                then().
                extract().
                response();

//        GET TRIP ID FROM RESPONSE
//                System.out.println(enquiryId);
        return Post.path("data.id");
    }

    public static void GetActiveTripForPassengerFail(String loginToken, String deviceToken) {
        String url = Url.base;
        String activeTrip = Url.passengerActiveTrip;

        given().
                relaxedHTTPSValidation().
                header("Authorization", ("Bearer " + loginToken)).
                header("X-Device-Token", deviceToken).
                expect().
                statusCode(404).
                log().all().
                when().
                get(url + activeTrip).
                then().
                extract().
                response();

//        GET TRIP ID FROM RESPONSE
//                System.out.println(enquiryId);
    }

    public static int GetPassengerTrip(String loginToken, String deviceToken, int Id) {
        String url = Url.base;
        String activeTrip = Url.passengerTrips;

        Response Post;
        Post = given().
                relaxedHTTPSValidation().
                header("Authorization", ("Bearer " + loginToken)).
                header("X-Device-Token", deviceToken).
                param("id", Id).
                expect().
                statusCode(200).
//                log().all().
        when().
                        get(url + activeTrip + Id).
                        then().
                        extract().
                        response();

//        GET TRIP ID FROM RESPONSE
//                System.out.println(enquiryId);
        return Post.path("data.user_id");
    }

    public UpdatePassengerTripResp UpdatePassengerTrip(String loginToken, String deviceToken, int tripId, int driverTripId) {
        String url = Url.base;
        String urlTrip = Url.passengerTrips;

        PassengerTripInfo putTrip = new PassengerTripInfo();
        putTrip.setId(tripId);
        putTrip.setDriver_trip_id(driverTripId);
        putTrip.setStart_pos(StaticVariables.startLng2 + "," + StaticVariables.startLat2);
        putTrip.setFinish_pos(StaticVariables.endLng2 + "," + StaticVariables.endLat2);
        putTrip.setStart_location(StaticVariables.address1);
        putTrip.setFinish_location(StaticVariables.address2);
        putTrip.setDistance(12345);

        Response response;
        response = given().
                relaxedHTTPSValidation().
                header("Authorization", ("Bearer " + loginToken)).
                header("X-Device-Token", deviceToken).
                body(putTrip).
                contentType(ContentType.JSON).
                expect().
                statusCode(200).
//                log().all().
        when().
//                contentType(ContentType.JSON).
        put(url + urlTrip + tripId);
        return response.as(UpdatePassengerTripResp.class);
    }

    public class PassengerTripInfo {
        private int id;
        private int driver_trip_id;
        private String start_pos;
        private String finish_pos;
        private String start_location;
        private String finish_location;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getDriver_trip_id() {
            return driver_trip_id;
        }

        private void setDriver_trip_id(int driver_trip_id) {
            this.driver_trip_id = driver_trip_id;
        }

        public String getStart_pos() {
            return start_pos;
        }

        public void setStart_pos(String start_pos) {
            this.start_pos = start_pos;
        }

        public String getFinish_pos() {
            return finish_pos;
        }

        public void setFinish_pos(String finish_pos) {
            this.finish_pos = finish_pos;
        }

        public String getStart_location() {
            return start_location;
        }

        public void setStart_location(String start_location) {
            this.start_location = start_location;
        }

        public String getFinish_location() {
            return finish_location;
        }

        public void setFinish_location(String finish_location) {
            this.finish_location = finish_location;
        }

        public int getDistance() {
            return distance;
        }

        public void setDistance(int distance) {
            this.distance = distance;
        }

        private int distance;
    }
}
