
package ua.com.passengerTrip.GSON.updatePassangerTripResp;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdatePassengerTripResp implements Serializable
{

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("data")
    @Expose
    private Data data;
    private final static long serialVersionUID = -3462351040342215808L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public UpdatePassengerTripResp() {
    }

    /**
     * 
     * @param data
     * @param success
     */
    public UpdatePassengerTripResp(Boolean success, Data data) {
        super();
        this.success = success;
        this.data = data;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

}
