
package ua.com.passengerTrip.GSON.updatePassangerTripResp;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data implements Serializable
{

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("driver_trip_id")
    @Expose
    private Integer driverTripId;
    @SerializedName("start_pos")
    @Expose
    private String startPos;
    @SerializedName("finish_pos")
    @Expose
    private String finishPos;
    @SerializedName("start_location")
    @Expose
    private String startLocation;
    @SerializedName("finish_location")
    @Expose
    private String finishLocation;
    @SerializedName("distance")
    @Expose
    private Integer distance;
    @SerializedName("started_at")
    @Expose
    private Integer startedAt;
    @SerializedName("finished_at")
    @Expose
    private Object finishedAt;
    private final static long serialVersionUID = -664261231751179931L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Data() {
    }

    /**
     * 
     * @param finishPos
     * @param id
     * @param distance
     * @param finishLocation
     * @param startPos
     * @param finishedAt
     * @param userId
     * @param startedAt
     * @param driverTripId
     * @param startLocation
     */
    public Data(Integer id, Integer userId, Integer driverTripId, String startPos, String finishPos, String startLocation, String finishLocation, Integer distance, Integer startedAt, Object finishedAt) {
        super();
        this.id = id;
        this.userId = userId;
        this.driverTripId = driverTripId;
        this.startPos = startPos;
        this.finishPos = finishPos;
        this.startLocation = startLocation;
        this.finishLocation = finishLocation;
        this.distance = distance;
        this.startedAt = startedAt;
        this.finishedAt = finishedAt;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getDriverTripId() {
        return driverTripId;
    }

    public void setDriverTripId(Integer driverTripId) {
        this.driverTripId = driverTripId;
    }

    public String getStartPos() {
        return startPos;
    }

    public void setStartPos(String startPos) {
        this.startPos = startPos;
    }

    public String getFinishPos() {
        return finishPos;
    }

    public void setFinishPos(String finishPos) {
        this.finishPos = finishPos;
    }

    public String getStartLocation() {
        return startLocation;
    }

    public void setStartLocation(String startLocation) {
        this.startLocation = startLocation;
    }

    public String getFinishLocation() {
        return finishLocation;
    }

    public void setFinishLocation(String finishLocation) {
        this.finishLocation = finishLocation;
    }

    public Integer getDistance() {
        return distance;
    }

    public void setDistance(Integer distance) {
        this.distance = distance;
    }

    public Integer getStartedAt() {
        return startedAt;
    }

    public void setStartedAt(Integer startedAt) {
        this.startedAt = startedAt;
    }

    public Object getFinishedAt() {
        return finishedAt;
    }

    public void setFinishedAt(Object finishedAt) {
        this.finishedAt = finishedAt;
    }

}
