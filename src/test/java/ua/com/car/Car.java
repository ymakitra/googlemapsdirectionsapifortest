package ua.com.car;


import io.restassured.http.ContentType;
import io.restassured.response.Response;
import ua.com.Url;
import ua.com.car.carInfoResp.CarInfoResp;
import ua.com.staticScenarios.StaticVariables;

import java.io.File;

import static io.restassured.RestAssured.given;

public class Car {
    public Car() {
    }

    public static String AllCars(String loginToken, String deviceToken) {
        String url = Url.base;
        String urlTwo = Url.cars;

        Response post;
        post = given().
                relaxedHTTPSValidation().
                header("Authorization", ("Bearer " + loginToken)).
                header("X-Device-Token", deviceToken).
                expect().
                statusCode(200).
//                log().all().
                when().
                get(url + urlTwo).
                then().
                extract().
                response();
        return post.path("data.items[0].make");
    }

    public static int CreateCar(String loginToken, String deviceToken, String make, String model, String plateNumber, String color, int seats) {
        String url = Url.base;
        String urlTwo = Url.cars;

        Response post;
        post = given().
                relaxedHTTPSValidation().
                header("Authorization", ("Bearer " + loginToken)).
                header("X-Device-Token", deviceToken).
                param("make", make).
                param("model", model).
                param("plate_number", plateNumber).
                param("color", color).
                param("seats", seats).
                expect().
                statusCode(201).
//                log().all().
        when().
                        post(url + urlTwo).
                        then().
                        extract().
                        response();
        return post.path("data.id");
    }

    public static void DeleteCars(String loginToken, String deviceToken, int carId) {
        String url = Url.base;
        String urlTwo = Url.cars;

        given().
                relaxedHTTPSValidation().
                header("Authorization", ("Bearer " + loginToken)).
                header("X-Device-Token", deviceToken).
                param("id", carId).
                expect().
                statusCode(204).
//                log().all().
                when().
                delete(url + urlTwo + "/" + carId).
                then().
                extract().
                response();
    }

    public static CarInfoResp GetCar(String loginToken, String deviceToken, int carId) {
        String url = Url.base;
        String urlTwo = Url.cars;

        Response response;
        response = given().
                relaxedHTTPSValidation().
                header("Authorization", ("Bearer " + loginToken)).
                header("X-Device-Token", deviceToken).
                param("id", carId).
                expect().
                statusCode(200).
//                log().all().
                when().
                get(url + urlTwo).
                then().
                extract().
                response();
        CarInfoResp dataFromResp = response.as(CarInfoResp.class);
//        System.out.println(dataFromResp.toString());
//        System.out.println(returnedArtworks);
//        System.out.println(masterpiece.getData().getItems().get(0).getColor());
        return dataFromResp;
    }

    public static String UploadCarPhoto(String loginToken, String deviceToken, int carId) {
        String url = Url.base;
        String urlTwo = Url.cars;
        String urlTree = Url.photo;
        String photo = StaticVariables.photoLink;

        Response post;
        post = given().
                relaxedHTTPSValidation().
                header("Authorization", ("Bearer " + loginToken)).
                header("X-Device-Token", deviceToken).
                param("id", carId).
                multiPart("photo", new File(photo)).
                expect().
                statusCode(200).
//                log().all().
        when().
                        post(url + urlTwo + "/" + carId + urlTree).
                        then().
                        extract().
                        response();
        return post.path("data.photo");
    }

    public void UpdateCar(String loginToken, String deviceToken, String make, String model, String plateNumber, String color, int seats, int carId) {
        String url = Url.base;
        String urlTwo = Url.cars;
        CarInfo post = new CarInfo();
        post.setMake(make);
        post.setModel(model);
        post.setPlate_number(plateNumber);
        post.setColor(color);
        post.setSeats(seats);

        given().
                relaxedHTTPSValidation().
                header("Authorization", ("Bearer " + loginToken)).
                header("X-Device-Token", deviceToken).
                body(post).
//                log().all().
        when().
                contentType(ContentType.JSON).
                put(url + urlTwo + "/" + carId);
    }

    public class CarInfo {

        private String make;
        private String model;
        private String plate_number;
        private String color;
        private int seats;


        void setMake(String make) {
            this.make = make;
        }

        public String getModel() {
            return model;
        }

        public void setModel(String model) {
            this.model = model;
        }

        void setPlate_number(String plate_number) {
            this.plate_number = plate_number;
        }

        void setColor(String color) {
            this.color = color;
        }

        void setSeats(int seats) {
            this.seats = seats;
        }
    }
}



