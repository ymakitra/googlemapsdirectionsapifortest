package ua.com.car;


import io.restassured.response.Response;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ua.com.Url;
import ua.com.authorization.Authorization;
import ua.com.car.carInfoResp.CarInfoResp;
import ua.com.staticScenarios.StaticVariables;
import java.util.ArrayList;

import static io.restassured.RestAssured.given;
import static org.testng.Assert.assertEquals;


public class CarExperiment {
    private String driverDevToken = StaticVariables.deviceToken01;
    private ArrayList<String> driverTokenList = new ArrayList<>();


    private static CarInfoResp GetCar(String loginToken, String deviceToken, int carId) {
        String url = Url.base;
        String urlTwo = Url.cars;

        Response response;
        response = given().
                relaxedHTTPSValidation().
                header("Authorization", ("Bearer " + loginToken)).
                header("X-Device-Token", deviceToken).
                param("id", carId).
                expect().
                statusCode(200).
                log().all().
        when().
                        get(url + urlTwo).
                        then().
                        extract().
                        response();
        CarInfoResp masterpiece = response.as(CarInfoResp.class);
        System.out.println(masterpiece.toString());
//        System.out.println(returnedArtworks);
//        System.out.println("seeef" + masterpiece.getData().getItems().get(0).getColor());
        return  masterpiece;

//        return post.path("data.items[0].plate_number");
    }

    @Test
    public void CarExperiment2() {
        CarExperiment.GetCar(driverTokenList.get(0), driverDevToken, 43);

    }

    @BeforeMethod
    public void BeforeTest() {
        System.out.println("CarExperiment");

//        GET login Tokens
        String driverToken = Authorization.Login(StaticVariables.email4, StaticVariables.password);
        driverTokenList.add(driverToken);
        System.out.println("  ");
    }

    @AfterMethod
    public void AfterTest() {
//        Logout
        String logoutMassage01 = Authorization.Logout(driverTokenList.get(0));
        final String expected = "Logout successful";
        assertEquals(logoutMassage01, expected);}
}

