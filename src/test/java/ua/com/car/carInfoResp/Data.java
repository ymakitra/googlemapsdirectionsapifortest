
package ua.com.car.carInfoResp;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data implements Serializable
{

    @SerializedName("items")
    @Expose
    private List<Item> items = null;
    @SerializedName("_links")
    @Expose
    private Links links;
    @SerializedName("_meta")
    @Expose
    private Meta meta;
    private final static long serialVersionUID = 7898571056673758062L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Data() {
    }

    /**
     * 
     * @param items
     * @param links
     * @param meta
     */
    public Data(List<Item> items, Links links, Meta meta) {
        super();
        this.items = items;
        this.links = links;
        this.meta = meta;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public Links getLinks() {
        return links;
    }

    public void setLinks(Links links) {
        this.links = links;
    }

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

}
