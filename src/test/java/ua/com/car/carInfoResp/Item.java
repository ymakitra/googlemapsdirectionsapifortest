
package ua.com.car.carInfoResp;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Item implements Serializable
{

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("make")
    @Expose
    private String make;
    @SerializedName("model")
    @Expose
    private String model;
    @SerializedName("plate_number")
    @Expose
    private String plateNumber;
    @SerializedName("color")
    @Expose
    private String color;
    @SerializedName("photo")
    @Expose
    private Object photo;
    @SerializedName("tmp_photo")
    @Expose
    private Object tmpPhoto;
    @SerializedName("seats")
    @Expose
    private Integer seats;
    @SerializedName("is_default")
    @Expose
    private Integer isDefault;
    private final static long serialVersionUID = -8504023765018560145L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Item() {
    }

    /**
     * 
     * @param tmpPhoto
     * @param id
     * @param model
     * @param isDefault
     * @param color
     * @param userId
     * @param seats
     * @param plateNumber
     * @param photo
     * @param make
     */
    public Item(Integer id, Integer userId, String make, String model, String plateNumber, String color, Object photo, Object tmpPhoto, Integer seats, Integer isDefault) {
        super();
        this.id = id;
        this.userId = userId;
        this.make = make;
        this.model = model;
        this.plateNumber = plateNumber;
        this.color = color;
        this.photo = photo;
        this.tmpPhoto = tmpPhoto;
        this.seats = seats;
        this.isDefault = isDefault;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Object getPhoto() {
        return photo;
    }

    public void setPhoto(Object photo) {
        this.photo = photo;
    }

    public Object getTmpPhoto() {
        return tmpPhoto;
    }

    public void setTmpPhoto(Object tmpPhoto) {
        this.tmpPhoto = tmpPhoto;
    }

    public Integer getSeats() {
        return seats;
    }

    public void setSeats(Integer seats) {
        this.seats = seats;
    }

    public Integer getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(Integer isDefault) {
        this.isDefault = isDefault;
    }

}
