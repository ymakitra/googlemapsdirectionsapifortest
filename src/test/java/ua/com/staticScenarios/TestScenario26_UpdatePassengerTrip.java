package ua.com.staticScenarios;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ua.com.authorization.Authorization;
import ua.com.driverTrip.DriverTrip;
import ua.com.passengerTrip.GSON.updatePassangerTripResp.UpdatePassengerTripResp;
import ua.com.passengerTrip.PassengerTrip;
import ua.com.pickup.Sent;

import java.util.ArrayList;
import static org.testng.Assert.assertEquals;

public class TestScenario26_UpdatePassengerTrip {
    private String driverDevToken = StaticVariables.deviceToken01;
    private String passengerDevToken  = StaticVariables.deviceToken02;
    private ArrayList<String> driverTokenList = new ArrayList<>();
    private ArrayList<String> passengerTokenList = new ArrayList<>();

    @BeforeMethod
    public void BeforeTest() {
//        GET login Tokens
        String driverToken = Authorization.Login(StaticVariables.email1, StaticVariables.password);
        driverTokenList.add(driverToken);
        String passengerToken = Authorization.Login(StaticVariables.email2, StaticVariables.password);
        passengerTokenList.add(passengerToken);
        System.out.println("  ");
    }


    @AfterMethod
    public void AfterTest() {
//        Finish driver trip
        int tripId = DriverTrip.GetActiveTripForDriver(driverTokenList.get(0), driverDevToken);
        DriverTrip.FinishDriverTrip(driverTokenList.get(0), driverDevToken, tripId);
//        Logout
        String logoutMassage01 = Authorization.Logout(driverTokenList.get(0));
        final String expected = "Logout successful";
        assertEquals(logoutMassage01, expected);
    }

    @Test
    public void GetIntersectedTrips() {
        System.out.println("TestScenario26 - Update Passenger trip");

//        GET Trip ID
        int driverTripId = DriverTrip.CreateTrip(StaticVariables.startLat, StaticVariables.startLng, StaticVariables.endLat, StaticVariables.endLng, driverTokenList.get(0),  StaticVariables.carId01, driverDevToken);

//        Send enquiry to the driver
        int enquiryId = Sent.EnquiryToTheDriverSuccess(driverTripId, StaticVariables.startLat, StaticVariables.startLng, StaticVariables.endLat, StaticVariables.endLng, passengerTokenList.get(0), passengerDevToken);

//        Send accept response to the passenger
        final String actual = Sent.AcceptResponseToThePassenger(driverTokenList.get(0), enquiryId, driverDevToken);
        final String expected = "You accepted enquiry from " + StaticVariables.userFirstName02;
        assertEquals(actual, expected);

//        Get Active Trip For Passenger
        int  passengerTripId = PassengerTrip.GetActiveTripForPassengerSuccess(passengerTokenList.get(0), passengerDevToken);

//        Update passenger Trip
        PassengerTrip passengerTrip = new PassengerTrip();
        UpdatePassengerTripResp respData = passengerTrip.UpdatePassengerTrip(passengerTokenList.get(0), passengerDevToken, passengerTripId, driverTripId);



//        UpdateDriverTripResp respData = driverTrip.UpdateDriverTrip(driverTokenList.get(0), driverDevToken, driverTripId, StaticVariables.carId03);
        assertEquals(respData.getData().getStartPos(), (StaticVariables.startLng2 + "," + StaticVariables.startLat2));
        assertEquals(respData.getData().getFinishPos(), (StaticVariables.endLng2 + "," + StaticVariables.endLat2));
        assertEquals(respData.getData().getStartLocation(),StaticVariables.address1);
        assertEquals(respData.getData().getFinishLocation(),StaticVariables.address2);
    }
}
