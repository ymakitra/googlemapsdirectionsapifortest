package ua.com.staticScenarios;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ua.com.authorization.Authorization;
import ua.com.service.DB;
import java.sql.SQLException;
import java.util.ArrayList;

public class TestScenario12_ChangePassword {
    private ArrayList<String> driverTokenList1 = new ArrayList<>();
    private ArrayList<String> driverTokenList2 = new ArrayList<>();

    @BeforeMethod
    public void BeforeTest() {
        System.out.println(" ");
    }

    @AfterMethod
    public void AfterTest() {
    }

    @Test
    public void ChangePassword() throws SQLException {
        System.out.println("TestScenario12");
//        Reset user password
        Authorization.ResetPassword(StaticVariables.email0);
        String resetTokenFromDB = DB.ResetToken();
        System.out.println(resetTokenFromDB);

//        Change user Password
        Authorization.ChangePassword(resetTokenFromDB, StaticVariables.password2, StaticVariables.password2);

//        Login with new Password
        driverTokenList1.add(Authorization.Login(StaticVariables.email0, StaticVariables.password2));

//        Logout
        Authorization.Logout(driverTokenList1.get(0));

//        Reset user password
        Authorization.ResetPassword(StaticVariables.email0);
        String resetTokenFromDB2 = DB.ResetToken();
        System.out.println(resetTokenFromDB2);

//        Change user Password to Old
        Authorization.ChangePassword(resetTokenFromDB2, StaticVariables.password, StaticVariables.password);

//        Login with old Password
        driverTokenList2.add(Authorization.Login(StaticVariables.email0, StaticVariables.password));

//        Logout
        Authorization.Logout(driverTokenList2.get(0));
    }
}
