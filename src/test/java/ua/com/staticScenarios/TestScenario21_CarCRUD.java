package ua.com.staticScenarios;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ua.com.authorization.Authorization;
import ua.com.car.Car;
import ua.com.car.carInfoResp.CarInfoResp;

import java.util.ArrayList;

import static org.testng.Assert.assertEquals;

public class TestScenario21_CarCRUD {
    private ArrayList<String> driverTokenList = new ArrayList<>();

    @BeforeMethod
    public void BeforeTest() {
        System.out.println("TestScenario21");

//        GET login Tokens
        String driverToken = Authorization.Login(StaticVariables.email4, StaticVariables.password);
        driverTokenList.add(driverToken);
        System.out.println("  ");
    }

    @AfterMethod
    public void AfterTest() {
//        Logout
        String logoutMassage01 = Authorization.Logout(driverTokenList.get(0));
        final String expected = "Logout successful";
        assertEquals(logoutMassage01, expected);
    }

    @Test
    public void CarCRUD() {
//        Create car
        int newCarId = Car.CreateCar(driverTokenList.get(0), StaticVariables.deviceToken04, StaticVariables.make, StaticVariables.model, StaticVariables.plateNumber, StaticVariables.color, StaticVariables.seats);
        CarInfoResp carData = Car.GetCar(driverTokenList.get(0), StaticVariables.deviceToken04, newCarId);
        int seats = carData.getData().getItems().get(0).getSeats();
        assertEquals(carData.getData().getItems().get(0).getMake(), StaticVariables.make);
        assertEquals(carData.getData().getItems().get(0).getModel(), StaticVariables.model);
        assertEquals(carData.getData().getItems().get(0).getPlateNumber(), StaticVariables.plateNumber);
        assertEquals(carData.getData().getItems().get(0).getColor(), StaticVariables.color);
        assertEquals(seats, StaticVariables.seats);

//        Update car
        System.out.println("Update car");
        Car car = new Car();
        car.UpdateCar(driverTokenList.get(0), StaticVariables.deviceToken04, StaticVariables.make2, StaticVariables.model2, StaticVariables.plateNumber2, StaticVariables.color2, StaticVariables.seats2, newCarId);
        CarInfoResp carData2 = Car.GetCar(driverTokenList.get(0), StaticVariables.deviceToken04, newCarId);
        int seats2 = carData2.getData().getItems().get(0).getSeats();
        assertEquals(carData2.getData().getItems().get(0).getMake(), StaticVariables.make2);
        assertEquals(carData2.getData().getItems().get(0).getModel(), StaticVariables.model2);
        assertEquals(carData2.getData().getItems().get(0).getPlateNumber(), StaticVariables.plateNumber2);
        assertEquals(carData2.getData().getItems().get(0).getColor(), StaticVariables.color2);
        assertEquals(seats2, StaticVariables.seats2);

//        Delete car
        Car.DeleteCars(driverTokenList.get(0),StaticVariables.deviceToken04, newCarId);
    }
}

