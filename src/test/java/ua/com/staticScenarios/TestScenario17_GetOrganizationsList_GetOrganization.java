package ua.com.staticScenarios;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ua.com.authorization.Authorization;
import ua.com.organization.Organization;

import java.util.ArrayList;

import static org.testng.Assert.assertEquals;

public class TestScenario17_GetOrganizationsList_GetOrganization {
    private String driverDevToken = StaticVariables.deviceToken04;
    private ArrayList<String> driverTokenList = new ArrayList<>();

    @BeforeMethod
    public void BeforeTest() {
//        GET login Tokens
        String driverToken = Authorization.Login(StaticVariables.email1, StaticVariables.password);
        driverTokenList.add(driverToken);
        System.out.println("  ");
    }

    @AfterMethod
    public void AfterTest() {
//        Logout
        String logoutMassage01 = Authorization.Logout(driverTokenList.get(0));
        final String expected = "Logout successful";
        assertEquals(logoutMassage01, expected);
    }

    @Test
    public void GetOrganizationsList_GetOrganization() {
        System.out.println("TestScenario17");
        String sort = "DESC";
        String expandParams = "type";
        int orgId = Organization.OrganizationsList(driverTokenList.get(0), driverDevToken, sort, expandParams);

        String orgName = Organization.GetOrganization(driverTokenList.get(0), driverDevToken, orgId, expandParams);
        final String expected = "Botswana Meat Commission";
        assertEquals(orgName, expected);
    }
}
