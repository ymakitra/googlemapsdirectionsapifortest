package ua.com.staticScenarios;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ua.com.authorization.Authorization;
import ua.com.driverTrip.DriverTrip;
import ua.com.driverTrip.gson.updateTripResp.UpdateDriverTripResp;

import java.util.ArrayList;

import static org.testng.Assert.assertEquals;

public class TestScenario16_UpdateDriverTrip {
    private String driverDevToken = StaticVariables.deviceToken04;
    private ArrayList<String> driverTokenList = new ArrayList<>();

    @BeforeMethod
    public void BeforeTest() {
//        GET login Tokens
        String driverToken = Authorization.Login(StaticVariables.email3, StaticVariables.password);
        driverTokenList.add(driverToken);
        System.out.println("  ");
    }

    @AfterMethod
    public void AfterTest() {
//        Finish driver trip
        int tripId = DriverTrip.GetActiveTripForDriver(driverTokenList.get(0), driverDevToken);
        DriverTrip.FinishDriverTrip(driverTokenList.get(0), driverDevToken, tripId);
//        Logout
        String logoutMassage01 = Authorization.Logout(driverTokenList.get(0));
        final String expected = "Logout successful";
        assertEquals(logoutMassage01, expected);
    }

    @Test
    public void GetIntersectedTrips() {
        System.out.println("TestScenario16 - Update trip");
//        GET Trip ID
        int driverTripId = DriverTrip.CreateTrip(StaticVariables.startLat, StaticVariables.startLng, StaticVariables.endLat, StaticVariables.endLng, driverTokenList.get(0), StaticVariables.carId03, driverDevToken);

//        Update Driver Trip
        DriverTrip driverTrip = new DriverTrip();
        UpdateDriverTripResp respData = driverTrip.UpdateDriverTrip(driverTokenList.get(0), driverDevToken, driverTripId, StaticVariables.carId03);
        assertEquals(respData.getData().getStartPos(), (StaticVariables.startLng2 + "," + StaticVariables.startLat2));
        assertEquals(respData.getData().getFinishPos(), (StaticVariables.endLng2 + "," + StaticVariables.endLat2));
        assertEquals(respData.getData().getStartLocation(),StaticVariables.address1);
        assertEquals(respData.getData().getFinishLocation(),StaticVariables.address2);
    }
}
