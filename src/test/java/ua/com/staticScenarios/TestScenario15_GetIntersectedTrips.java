package ua.com.staticScenarios;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ua.com.authorization.Authorization;
import ua.com.driverTrip.DriverTrip;
import java.util.ArrayList;
import static org.testng.Assert.assertEquals;

public class TestScenario15_GetIntersectedTrips {
    private String driverDevToken = StaticVariables.deviceToken04;
    private ArrayList<String> driverTokenList = new ArrayList<>();

    @BeforeMethod
    public void BeforeTest() {
//        GET login Tokens
        String driverToken = Authorization.Login(StaticVariables.email4, StaticVariables.password);
        driverTokenList.add(driverToken);
        System.out.println("  ");
    }

    @AfterMethod
    public void AfterTest() {
//        Logout
        String logoutMassage01 = Authorization.Logout(driverTokenList.get(0));
        final String expected = "Logout successful";
        assertEquals(logoutMassage01, expected);
    }

    @Test
    public void GetIntersectedTrips() {
        System.out.println("TestScenario15");
        DriverTrip.GetIntersectedTrips(driverTokenList.get(0), driverDevToken);
    }
}
