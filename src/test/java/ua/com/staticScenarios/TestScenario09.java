package ua.com.staticScenarios;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ua.com.authorization.Authorization;
import ua.com.driverTrip.DriverTrip;
import ua.com.pickup.Sent;
import java.util.ArrayList;
import static org.testng.Assert.assertEquals;

public class TestScenario09 {
    private String driverDevToken = StaticVariables.deviceToken01;
    private String passengerDevToken1 = StaticVariables.deviceToken02;
    private String passengerDevToken2 = StaticVariables.deviceToken03;
    private ArrayList<String> driverTokenList = new ArrayList<>();
    private ArrayList<String> passengerTokenList1 = new ArrayList<>();
    private ArrayList<String> passengerTokenList2 = new ArrayList<>();


    @BeforeMethod
    public void BeforeTest() {
//        GET login Tokens
        String driverToken = Authorization.Login(StaticVariables.email1, StaticVariables.password);
        driverTokenList.add(driverToken);
        String passengerToken1 = Authorization.Login(StaticVariables.email2, StaticVariables.password);
        passengerTokenList1.add(passengerToken1);
        String passengerToken2 = Authorization.Login(StaticVariables.email3, StaticVariables.password);
        passengerTokenList2.add(passengerToken2);
        System.out.println("  ");
    }

    @AfterMethod
    public void AfterTest() {
//        Finish driver trip
        int tripId = DriverTrip.GetActiveTripForDriver(driverTokenList.get(0), driverDevToken);
        DriverTrip.FinishDriverTrip(driverTokenList.get(0), driverDevToken, tripId);
//        Logout
        String logoutMassage01 = Authorization.Logout(driverTokenList.get(0));
        String logoutMassage02 = Authorization.Logout(passengerTokenList1.get(0));
        String logoutMassage03 = Authorization.Logout(passengerTokenList2.get(0));
        final String expected = "Logout successful";
        assertEquals(logoutMassage01, expected);
        assertEquals(logoutMassage02, expected);
        assertEquals(logoutMassage03, expected);
    }

    @Test
    public void SendEnquiry_SendEnquiry() {
        System.out.println("TestScenario09");

//        GET Trip ID
        int driverTripId = DriverTrip.CreateTrip(StaticVariables.startLat, StaticVariables.startLng, StaticVariables.endLat, StaticVariables.endLng, driverTokenList.get(0),  StaticVariables.carId01, driverDevToken);
        System.out.println(driverTripId);

//        Send enquiry to the driver from passenger 1
        Sent.EnquiryToTheDriverSuccess(driverTripId, StaticVariables.startLat, StaticVariables.startLng, StaticVariables.endLat, StaticVariables.endLng, passengerTokenList1.get(0), passengerDevToken1);

//        Send enquiry to the driver from passenger 2
        String massage = Sent.EnquiryToTheDriverFalse(driverTripId, StaticVariables.startLat, StaticVariables.startLng, StaticVariables.endLat, StaticVariables.endLng, passengerTokenList2.get(0), passengerDevToken2);

        final String expected = "Driver has another request";
        assertEquals(massage, expected);
//        System.out.println("Massage : " + massage);
    }
}
