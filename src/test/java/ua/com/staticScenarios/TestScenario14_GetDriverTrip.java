package ua.com.staticScenarios;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ua.com.authorization.Authorization;
import ua.com.driverTrip.DriverTrip;

import java.util.ArrayList;

import static org.testng.Assert.assertEquals;

public class TestScenario14_GetDriverTrip {
        private String driverDevToken = StaticVariables.deviceToken01;
        private ArrayList<String> driverTokenList = new ArrayList<>();

        @BeforeMethod
        public void BeforeTest() {
//        GET login Tokens
            String driverToken = Authorization.Login(StaticVariables.email1, StaticVariables.password);
            driverTokenList.add(driverToken);
            System.out.println("  ");
        }

        @AfterMethod
        public void AfterTest() {
//        Finish driver trip
//            int tripId = DriverTrip.GetActiveTripForDriver(driverTokenList.get(0), driverDevToken);
//            DriverTrip.FinishDriverTrip(driverTokenList.get(0), driverDevToken, tripId);

//        Logout
            String logoutMassage01 = Authorization.Logout(driverTokenList.get(0));
            final String expected = "Logout successful";
            assertEquals(logoutMassage01, expected);

        }

        @Test
        public void GetDriverTrip() {
            System.out.println("TestScenario14");

//            GET Trip ID
            int driverTripId = 882;
//                    DriverTrip.CreateTrip(StaticVariables.startLat, StaticVariables.startLng, StaticVariables.endLat, StaticVariables.endLng, driverTokenList.get(0),  StaticVariables.carId01, driverDevToken);
            DriverTrip.GetDriverTrip(driverTokenList.get(0), driverDevToken, driverTripId);
        }
    }