package ua.com.staticScenarios;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ua.com.authorization.Authorization;
import ua.com.car.Car;

import java.util.ArrayList;

import static org.testng.Assert.assertEquals;

public class TestScenario20_AllCars {
    private String driverDevToken = StaticVariables.deviceToken01;
    private ArrayList<String> driverTokenList = new ArrayList<>();

    @BeforeMethod
    public void BeforeTest() {
        System.out.println("TestScenario20AllCars");
//        GET login Tokens
        String driverToken = Authorization.Login(StaticVariables.email1, StaticVariables.password);
        driverTokenList.add(driverToken);
        System.out.println("  ");
    }

    @AfterMethod
    public void AfterTest() {
//        Logout
        String logoutMassage01 = Authorization.Logout(driverTokenList.get(0));
        final String expected = "Logout successful";
        assertEquals(logoutMassage01, expected);
    }

    @Test
    public void GetAllCars() {
        String car = Car.AllCars(driverTokenList.get(0), driverDevToken);
//        System.out.println(car);
        assertEquals(car, "Mazda");
    }
}
