package ua.com.staticScenarios;

import org.testng.annotations.*;
import ua.com.authorization.Authorization;
import ua.com.driverTrip.*;
import ua.com.passengerTrip.PassengerTrip;
import ua.com.pickup.*;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import static org.testng.Assert.assertEquals;

public class TestScenario04 {
    private String driverDevToken = StaticVariables.deviceToken01;
    private String passengerDevToken  = StaticVariables.deviceToken02;
    private ArrayList<String> driverTokenList = new ArrayList<>();
    private ArrayList<String> passengerTokenList = new ArrayList<>();


    @BeforeMethod
    public void BeforeTest() {
//        GET login Tokens
        String driverToken = Authorization.Login(StaticVariables.email1, StaticVariables.password);
        driverTokenList.add(driverToken);
        String passengerToken = Authorization.Login(StaticVariables.email2, StaticVariables.password);
        passengerTokenList.add(passengerToken);
        System.out.println("  ");
    }

    @AfterMethod
    public void AfterTest() {
//        Finish driver trip
        int tripId = DriverTrip.GetActiveTripForDriver(driverTokenList.get(0), driverDevToken);
        DriverTrip.FinishDriverTrip(driverTokenList.get(0), driverDevToken, tripId);
//        Logout
        String logoutMassage01 = Authorization.Logout(driverTokenList.get(0));
        String logoutMassage02 = Authorization.Logout(passengerTokenList.get(0));
        final String expected = "Logout successful";
        assertEquals(logoutMassage01, expected);
        assertEquals(logoutMassage02, expected);
    }

    @Test
    public void SendEnquiry_AcceptResponse_CancelToDriver() throws InterruptedException {
        System.out.println("TestScenario04");

//        GET Trip ID
        int driverTripId = DriverTrip.CreateTrip(StaticVariables.startLat, StaticVariables.startLng, StaticVariables.endLat, StaticVariables.endLng, driverTokenList.get(0),  StaticVariables.carId01, driverDevToken);

//        Send enquiry to the driver
        int enquiryId = Sent.EnquiryToTheDriverSuccess(driverTripId, StaticVariables.startLat, StaticVariables.startLng, StaticVariables.endLat, StaticVariables.endLng, passengerTokenList.get(0), passengerDevToken);

//        Send accept response to the passenger
        Sent.AcceptResponseToThePassenger(driverTokenList.get(0), enquiryId, driverDevToken);

//        Get Active Trip For Passenger
        int passengerTripId = PassengerTrip.GetActiveTripForPassengerSuccess(passengerTokenList.get(0),passengerDevToken);

//        TimeUnit.SECONDS.sleep(4);
//
//        Send Cancel passenger trip
        String massage = PassengerTrip.CancelPassengerTrip(passengerTokenList.get(0), passengerDevToken, passengerTripId);


        final String expected = "You cancelled your lift with Auto1s1";
        assertEquals(massage, expected);

//        System.out.println("Cancel trip by passenger: " + massage);
        System.out.println(driverTripId);
    }
}
