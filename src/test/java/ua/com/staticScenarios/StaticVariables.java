package ua.com.staticScenarios;


public class StaticVariables {
    static String email0 = "y.makitra@relevant.software";
    static String email1 = "y.makitra1@relevant.software";
    static String email2 = "y.makitra2@relevant.software";
    static String email3 = "y.makitra3@relevant.software";
    static String email10 = "y.makitra10@relevant.software";
    public static String email4 = "y.makitra4@relevant.software";

    static String userFirstName02 = "Auto2fs2";
    static String userFirstName03 = "Auto3s3";

    public static String password = "12345678";
    static String password2 = "87654321";

    public static String deviceToken01 = "123456";
    static String deviceToken02 = "654321";
    static String deviceToken03 = "765432";
    static String deviceToken04 = "45678";
    static String deviceToken10 = "10";

    static int carId01 = 9;
    static int carId03 = 11;
    static int carId04 = 43;

    static String userId10 = "24";

    public static String startLat = "49.8445500";
    public static String startLng = "23.9970000";
    public static String endLat = "49.842754";
    public static String endLng = "24.009286";

    public static String startLat2 = "49.999000";
    public static String startLng2 = "23.9990000";
    public static String endLat2 = "49.777000";
    public static String endLng2 = "24.777000";

    static String make = "ZAZ Lacoste";
    static String model = "Rocket Star";
    static String plateNumber = "VS 5634 zx";
    static String color = "Strange blue";
    static int seats = 2;

    static String make2 = "Suzuki Swift";
    static String model2 = "New Swift";
    static String plateNumber2 = "AA 3652 ss";
    static String color2 = "Dump black";
    static int seats2 = 4;


    public static String address1 = "Gold str 90";
    public static String address2 = "Silver str 8";
    public static int distance = 1234;
    public static String photoLink = "C:/Users/YuriyMakitra/Let/src/main/resources/123.jpg";
}
