package ua.com.staticScenarios;

import org.testng.annotations.*;
import ua.com.authorization.Authorization;
import ua.com.driverTrip.*;
import ua.com.pickup.*;
import java.util.ArrayList;
import static org.testng.Assert.assertEquals;

public class TestScenario03 {
    private String driverDevToken = StaticVariables.deviceToken01;
    private String passengerDevToken  = StaticVariables.deviceToken02;
    private ArrayList<String> driverTokenList = new ArrayList<>();
    private ArrayList<String> passengerTokenList = new ArrayList<>();


    @BeforeMethod
    public void BeforeTest() {
//        GET login Tokens
        String driverToken = Authorization.Login(StaticVariables.email1, StaticVariables.password);
        driverTokenList.add(driverToken);
        String passengerToken = Authorization.Login(StaticVariables.email2, StaticVariables.password);
        passengerTokenList.add(passengerToken);
        System.out.println("  ");
    }

    @AfterMethod
    public void AfterTest() {
//        Finish driver trip
        int tripId = DriverTrip.GetActiveTripForDriver(driverTokenList.get(0), driverDevToken);
        DriverTrip.FinishDriverTrip(driverTokenList.get(0), driverDevToken, tripId);

//        Logout
        String logoutMassage01 = Authorization.Logout(driverTokenList.get(0));
        String logoutMassage02 = Authorization.Logout(passengerTokenList.get(0));
        final String expected = "Logout successful";
        assertEquals(logoutMassage01, expected);
        assertEquals(logoutMassage02, expected);
    }

    @Test
    public void SendEnquiry_CancelEnquiry() {
        System.out.println("TestScenario03");

//        GET Trip ID
        int driverTripId = DriverTrip.CreateTrip(StaticVariables.startLat, StaticVariables.startLng, StaticVariables.endLat, StaticVariables.endLng, driverTokenList.get(0),  StaticVariables.carId01, driverDevToken);

//        Send enquiry to the driver
        Sent.EnquiryToTheDriverSuccess(driverTripId, StaticVariables.startLat, StaticVariables.startLng, StaticVariables.endLat, StaticVariables.endLng, passengerTokenList.get(0), passengerDevToken);

//        Send Cancel response to the passenger

        final String actual = Sent.CancelMessageToTheDriver(passengerTokenList.get(0), passengerDevToken, driverTripId);
        final String expected = "You cancelled your lift with Auto1s1";
        assertEquals(actual, expected);

//        System.out.println("Cancel massage: " + massage);
        System.out.println(driverTripId);
    }
}
