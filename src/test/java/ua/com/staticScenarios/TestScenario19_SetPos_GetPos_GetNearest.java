package ua.com.staticScenarios;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ua.com.authorization.Authorization;
import ua.com.position.Position;
import java.util.ArrayList;
import static org.testng.Assert.assertEquals;

public class TestScenario19_SetPos_GetPos_GetNearest {

    private ArrayList<String> driverTokenList = new ArrayList<>();

    @BeforeMethod
    public void BeforeTest() {
//        GET login Tokens
        String driverToken = Authorization.Login(StaticVariables.email10, StaticVariables.password);
        driverTokenList.add(driverToken);
        System.out.println("  ");
    }

    @AfterMethod
    public void AfterTest() {
//        Logout
        String logoutMassage01 = Authorization.Logout(driverTokenList.get(0));
        final String expected = "Logout successful";
        assertEquals(logoutMassage01, expected);
    }

    @Test
    public void SetPosition_GetPosition_GetNearestDrivers() {
        System.out.println("TestScenario19");
        String message =  Position.SetDevicePosition(driverTokenList.get(0), StaticVariables.startLat, StaticVariables.startLng, 10, 50);
        assertEquals(message, "Position has been saved.");
        Position.GetDevicePosition(StaticVariables.userId10);
        Position.GetNearestDrivers(StaticVariables.startLat, StaticVariables.startLng);
        Position.RemoteDevicePosition(driverTokenList.get(0));
    }
}
