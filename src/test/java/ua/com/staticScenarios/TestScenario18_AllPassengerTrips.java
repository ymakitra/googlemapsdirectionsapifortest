package ua.com.staticScenarios;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ua.com.authorization.Authorization;
import ua.com.driverTrip.DriverTrip;
import ua.com.passengerTrip.PassengerTrip;
import java.util.ArrayList;

import static org.testng.Assert.assertEquals;

public class TestScenario18_AllPassengerTrips {
    private String driverDevToken = StaticVariables.deviceToken01;
    private String passengerDevToken  = StaticVariables.deviceToken10;
    private ArrayList<String> driverTokenList = new ArrayList<>();
    private ArrayList<String> passengerTokenList = new ArrayList<>();

    @BeforeMethod
    public void BeforeTest() {
//        GET login Tokens
        String driverToken = Authorization.Login(StaticVariables.email1, StaticVariables.password);
        driverTokenList.add(driverToken);
        String passengerToken = Authorization.Login(StaticVariables.email10, StaticVariables.password);
        passengerTokenList.add(passengerToken);
        System.out.println("  ");
    }

    @AfterMethod
    public void AfterTest() {
//        Finish driver trip
        int tripId = DriverTrip.GetActiveTripForDriver(driverTokenList.get(0), driverDevToken);
        DriverTrip.FinishDriverTrip(driverTokenList.get(0), driverDevToken, tripId);

//        Logout
        String logoutMassage01 = Authorization.Logout(driverTokenList.get(0));
        String logoutMassage02 = Authorization.Logout(passengerTokenList.get(0));
        final String expected = "Logout successful";
        assertEquals(logoutMassage01, expected);
        assertEquals(logoutMassage02, expected);
    }

    @Test
    public void AllPassengerTrips() {
        System.out.println("TestScenario18");

//        GET Trip ID
        int driverTripId = DriverTrip.CreateTrip(StaticVariables.startLat, StaticVariables.startLng, StaticVariables.endLat, StaticVariables.endLng, driverTokenList.get(0),  StaticVariables.carId01, driverDevToken);
        int passengerTripId = PassengerTrip.CreatePassengerTrip(passengerTokenList.get(0), passengerDevToken, driverTripId, StaticVariables.startLat, StaticVariables.startLng, StaticVariables.endLat, StaticVariables.endLng);

//        Get all trips
        String attributeValue = "";
        String attribute = "";
        String sort = "DESC";
        String expandParams = "";
        int userId = PassengerTrip.AllPassengerTrips(passengerTokenList.get(0), driverDevToken, attribute, attributeValue, sort, expandParams);
        final int expected = 24;
        assertEquals(userId, expected);

//        Cancel passenger trip
        PassengerTrip.CancelPassengerTrip(passengerTokenList.get(0), driverDevToken , passengerTripId);

    }
}
