//package ua.com.staticScenarios;
//
//import org.testng.annotations.AfterMethod;
//import org.testng.annotations.BeforeMethod;
//import org.testng.annotations.Test;
//import ua.com.Authorization.LoginUser;
//import ua.com.Authorization.LogoutUser;
//import ua.com.Variables;
//import ua.com.driverTrip.CreateTrip;
//import ua.com.driverTrip.FinishDriverTrip;
//import ua.com.driverTrip.GetActiveTripForDriver;
//
//
//import java.util.ArrayList;
//
//public class Scenario99 {
//
//    Variables variables = new Variables();
//    LoginUser loginUser = new LoginUser();
//    String driverDevToken = variables.getDeviceTokenOne();
//    String passengerDevToken = variables.getDeviceTokenTwo();
//    ArrayList<String> driverTokenList = new ArrayList<String>();
//    ArrayList<String> passengerTokenList = new ArrayList<String>();
//
//
//    @BeforeMethod
////    @BeforeClass
////    @BeforeTest
//    public void BeforeTest() {
//        System.out.println("Before 1");
////        GET login Tokens
//        String driverToken = loginUser.getLoginUser("y.makitra@relevant.software", "12345678");
//        driverTokenList.add(driverToken);
//
//        String passengerToken = loginUser.getLoginUser("y.makitra10@relevant.software", "12345678");
//        passengerTokenList.add(passengerToken);
//
//        System.out.println(driverToken +"  "+ passengerToken);
//        System.out.println("Before 2");
//    }
//
////    @Test
////    public void SendEnquiry_AcceptResponse() {
////
////        System.out.println("SendEnquiry_AcceptResponse1");
////        System.out.println(driverTokenList);
//////        GET Trip ID
////        CreateTrip id = new CreateTrip();
////        int driverTripId = id.CreateTrip(variables.getStartLat(), variables.getStartLng(), variables.getEndLat(), variables.getEndLng(), driverTokenList.get(0), variables.getCarId(), driverDevToken);
////
//////        Send enquiry to the driver
////        int enquiryId = SendEnquiryToTheDriver.SendEnquiryToTheDriver(driverTripId, variables.getStartLat(), variables.getStartLng(), variables.getEndLat(), variables.getEndLng(), passengerTokenList.get(0), passengerDevToken);
////
//////        Send accept response to the passenger
////        String massage = SendAcceptResponseToThePassenger.SendAcceptResponseToThePassenger(driverTokenList.get(0), enquiryId, driverDevToken);
////        System.out.println("Accept massage: " + massage);
////        System.out.println(driverTripId);
//////        System.out.println(driverTokenList.get(0) +"  "+passengerTokenList.get(0));
//////        System.out.println(driverTokenList);
////        System.out.println("SendEnquiry_AcceptResponse2");
////    }
////
////    @Test
////    public void SendEnquiry_DeclineResponse() {
////        System.out.println("SendEnquiry_DeclineResponse1");
////        System.out.println(driverTokenList);
//////        GET Trip ID
////        CreateTrip id = new CreateTrip();
////        int driverTripId = id.CreateTrip(variables.getStartLat(), variables.getStartLng(), variables.getEndLat(), variables.getEndLng(), driverTokenList.get(0), variables.getCarId(), driverDevToken);
////
//////        Send enquiry to the driver
////        int enquiryId = SendEnquiryToTheDriver.SendEnquiryToTheDriver(driverTripId, variables.getStartLat(), variables.getStartLng(), variables.getEndLat(), variables.getEndLng(), passengerTokenList.get(0), passengerDevToken);
////
//////        Send decline response to the passenger
////        String massage = SendDeclineResponseToThePassenger.SendDeclineResponseToThePassenger(driverTokenList.get(0), enquiryId, driverDevToken);
////        System.out.println("Decline massage: " + massage);
////        System.out.println(driverTripId);
//////        System.out.println(driverTokenList.get(0) +"  "+passengerTokenList.get(0));
//////        System.out.println(driverTokenList);
////        System.out.println("SendEnquiry_DeclineResponse2");
////    }
////
////    @Test
////    public void SendEnquiry_CancelEnquiry() {
//////        GET Trip ID
////        CreateTrip id = new CreateTrip();
////        int driverTripId = id.CreateTrip(variables.getStartLat(), variables.getStartLng(), variables.getEndLat(), variables.getEndLng(), driverTokenList.get(0), variables.getCarId(),driverDevToken);
////
//////        Send enquiry to the driver
////        SendEnquiryToTheDriver.SendEnquiryToTheDriver(driverTripId, variables.getStartLat(), variables.getStartLng(), variables.getEndLat(), variables.getEndLng(), passengerTokenList.get(0), passengerDevToken);
////
//////        Send Cancel response to the passenger
////        String massage = SendCancelMessageToTheDriver.SendCancelMessageToTheDriver(passengerTokenList.get(0), passengerDevToken, driverTripId);
////        System.out.println("Cancel massage: " + massage);
////        System.out.println(driverTripId);
////        System.out.println(driverTokenList.get(0) +"  "+passengerTokenList.get(0));
////        System.out.println(driverTokenList);
////    }
//
//    @AfterMethod
////    @AfterClass
////    @AfterTest
//    public void AfterTest() {
//        System.out.println("AFTER");
////        Finish driver trip
//        int tripId = GetActiveTripForDriver.GetActiveTripForDriver(driverTokenList.get(0), driverDevToken);
//        FinishDriverTrip.FinishDriverTrip(driverTokenList.get(0), driverDevToken, tripId);
//
//        LogoutUser.LogoutUser(driverTokenList.get(0));
//        System.out.println("dr");
//        LogoutUser.LogoutUser(passengerTokenList.get(0));
//        System.out.println("pas");
//    }
//}
