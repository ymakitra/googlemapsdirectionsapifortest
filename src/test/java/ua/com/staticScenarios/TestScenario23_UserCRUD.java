package ua.com.staticScenarios;

import org.testng.annotations.Test;
import ua.com.authorization.Authorization;
import ua.com.authorization.newUserResp.RegisterANewUserResp;
import ua.com.user.User;
import java.util.ArrayList;

public class TestScenario23_UserCRUD {
    private String driverDevToken = StaticVariables.deviceToken04;
    private ArrayList<String> driverTokenList = new ArrayList<>();

    @Test
    public void UserCRUD() {
        System.out.println("TestScenario23");

//        Register New User
        RegisterANewUserResp resp = Authorization.Register();
        int userId = resp.getData().getUser().getId();

//        GET login Tokens
        String driverToken = Authorization.Login(resp.getData().getUser().getEmail(), StaticVariables.password);
        driverTokenList.add(driverToken);

//        Get user
        User.GetUser(driverTokenList.get(0), driverDevToken,userId);

//        All user
        User.AllUsers(driverTokenList.get(0), driverDevToken, "id", String.valueOf(userId));

//        Delete user
        User.DeleteUser(driverTokenList.get(0), driverDevToken, userId);

//        User.AllUsers(driverTokenList.get(0), driverDevToken, "id", String.valueOf(userId));
    }
}