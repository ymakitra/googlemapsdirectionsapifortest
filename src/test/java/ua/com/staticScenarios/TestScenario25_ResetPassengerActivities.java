package ua.com.staticScenarios;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ua.com.authorization.Authorization;
import ua.com.driverTrip.DriverTrip;
import ua.com.passengerTrip.PassengerTrip;
import ua.com.pickup.Sent;
import ua.com.user.User;

import java.util.ArrayList;

import static org.testng.Assert.assertEquals;

public class TestScenario25_ResetPassengerActivities {
    private String driverDevToken = StaticVariables.deviceToken01;
    private String passengerDevToken = StaticVariables.deviceToken02;
    private ArrayList<String> driverTokenList = new ArrayList<>();
    private ArrayList<String> passengerTokenList = new ArrayList<>();

    @BeforeMethod
    public void BeforeTest() {
//        GET login Tokens
        String driverToken = Authorization.Login(StaticVariables.email1, StaticVariables.password);
        driverTokenList.add(driverToken);
        String passengerToken = Authorization.Login(StaticVariables.email3, StaticVariables.password);
        passengerTokenList.add(passengerToken);
        System.out.println("  ");
    }

    @AfterMethod
    public void AfterTest() {
//        Logout
        String logoutMassage01 = Authorization.Logout(driverTokenList.get(0));
        String logoutMassage02 = Authorization.Logout(passengerTokenList.get(0));
        final String expected = "Logout successful";
        assertEquals(logoutMassage01, expected);
        assertEquals(logoutMassage02, expected);
    }

    @Test
    public void ResetPassengerActivities() {
        System.out.println("TestScenario25");

//        GET Trip ID
        int driverTripId = DriverTrip.CreateTrip(StaticVariables.startLat, StaticVariables.startLng, StaticVariables.endLat, StaticVariables.endLng, driverTokenList.get(0), StaticVariables.carId01, driverDevToken);

//        Send enquiry to the driver
        int enquiryId = Sent.EnquiryToTheDriverSuccess(driverTripId, StaticVariables.startLat, StaticVariables.startLng, StaticVariables.endLat, StaticVariables.endLng, passengerTokenList.get(0), passengerDevToken);

//        Send accept response to the passenger
        final String actual = Sent.AcceptResponseToThePassenger(driverTokenList.get(0), enquiryId, driverDevToken);
        final String expected = "You accepted enquiry from " + StaticVariables.userFirstName03;
        assertEquals(actual, expected);

//        Get Active Trip For Driver/Passenger
        DriverTrip.GetActiveTripForDriver(driverTokenList.get(0), driverDevToken);
        PassengerTrip.GetActiveTripForPassengerSuccess(passengerTokenList.get(0), passengerDevToken);

//        Reset Passenger Activities
        String message = User.ResetUserActivities(passengerTokenList.get(0), passengerDevToken);
        assertEquals(message, "success");

//        Get active trip for Driver/Passenger
        PassengerTrip.GetActiveTripForPassengerFail(passengerTokenList.get(0), passengerDevToken);
        DriverTrip.GetActiveTripForDriver(driverTokenList.get(0), driverDevToken);
    }
}

