package ua.com.pickup;

import io.restassured.response.Response;
import ua.com.Url;
import ua.com.Variables;
import ua.com.service.RandomGenerator;

import static io.restassured.RestAssured.given;


public class Sent {

    public static String AcceptResponseToThePassenger(String loginToken, int enquiryId, String deviceToken) {
        String url = Url.base;
        String urlOne = Url.pickup;
        String urlTwo = Url.accept;

        Response Post;
        Post = given().
                relaxedHTTPSValidation().
                header("Authorization", ("Bearer " + loginToken)).
                header("X-Device-Token", deviceToken).
                param("id", enquiryId).
                expect().
                statusCode(200).
//                log().all().
        when().
                        post(url + urlOne + enquiryId + urlTwo).
                        then().
                        extract().
                        response();

        //        GET TRIP ID FROM RESPONSE
        //        System.out.println(enquiryId);
        return Post.path("data.message");
    }

    public static String EnquiryToTheDriverFalse(int tripId, String startLat, String startLng, String endLat, String endLng, String loginToken, String deviceToken) {
        String url = Url.base;
        String urlEnquiryOne = Url.pickup;
        String urlEnquiryTwo = Url.enquiry;

        Variables variables = new Variables();
        String distanceS = Integer.toString(RandomGenerator.IntRandomGenerator(variables.getDistanceMin(), variables.getDistanceMax()));

        Response Post;
        Post = given().
                relaxedHTTPSValidation().
                header("Authorization", ("Bearer " + loginToken)).
                header("X-Device-Token", deviceToken).
                param("id", tripId).
                param("start_pos", (startLat + ", " + startLng)).
                param("finish_pos", (endLat + ", " + endLng)).
                param("start_location", "Green Str").
                param("finish_location", "Red Str").
                param("distance", distanceS).
                param("seats", 1).
                expect().
                statusCode(422).
//                log().all().
        when().
                        post(url + urlEnquiryOne + tripId + urlEnquiryTwo).
                        then().
                        extract().
                        response();

        //        GET TRIP ID FROM RESPONSE
        //        System.out.println(enquiryId);
        return Post.path("data[0].message");
    }

    public static int EnquiryToTheDriverSuccess(int tripId, String startLat, String startLng, String endLat, String endLng, String loginToken, String deviceToken) {
        String url = Url.base;
        String urlEnquiryOne = Url.pickup;
        String urlEnquiryTwo = Url.enquiry;

        Variables variables = new Variables();
        String distanceS = Integer.toString(RandomGenerator.IntRandomGenerator(variables.getDistanceMin(), variables.getDistanceMax()));

        Response Post;
        Post = given().
                relaxedHTTPSValidation().
                header("Authorization", ("Bearer " + loginToken)).
                header("X-Device-Token", deviceToken).
                param("id", tripId).
                param("start_pos", (startLat + ", " + startLng)).
                param("finish_pos", (endLat + ", " + endLng)).
                param("start_location", "Green Str").
                param("finish_location", "Red Str").
                param("distance", distanceS).
                param("seats", 1).
                expect().
                statusCode(200).
//                log().all().
                when().
                post(url + urlEnquiryOne + tripId + urlEnquiryTwo).
                then().
                extract().
                response();

        //        GET TRIP ID FROM RESPONSE
        //        System.out.println(enquiryId);
        return Post.path("data.enquiry_id");
    }

    public static String DeclineResponseToThePassenger(String loginToken, int enquiryId, String deviceToken) {
        String url = Url.base;
        String urlOne = Url.pickup;
        String urlTwo = Url.decline;

        Response Post;
        Post = given().
                relaxedHTTPSValidation().
                header("Authorization", ("Bearer " + loginToken)).
                header("X-Device-Token", deviceToken).
                param("id", enquiryId).
                expect().
                statusCode(200).
//                log().all().
        when().
                        post(url + urlOne + enquiryId + urlTwo).
                        then().
                        extract().
                        response();

        //        GET TRIP ID FROM RESPONSE
        //        System.out.println(enquiryId);
        return Post.path("data.message");
    }

    public static String CancelMessageToTheDriver(String loginToken, String deviceToken, int tripId) {
        String url = Url.base;
        String urlOne = Url.pickup;
        String urlTwo = Url.cancel;

        Response Post;
        Post = given().
                relaxedHTTPSValidation().
                header("Authorization", ("Bearer " + loginToken)).
                header("X-Device-Token", deviceToken).
                param("id", tripId).
                expect().
                statusCode(200).
//                log().all().
        when().
                        post(url + urlOne + tripId + urlTwo).
                        then().
                        extract().
                        response();

        return Post.path("data.message");
    }

    public static String MessageToThePassengerThatDriverHasArrived(String loginToken, String deviceToken, int tripId) {
        String url = Url.base;
        String urlOne = Url.pickup;
        String urlTwo = Url.arrived;

        Response Post;
        Post = given().
                relaxedHTTPSValidation().
                header("Authorization", ("Bearer " + loginToken)).
                header("X-Device-Token", deviceToken).
                param("id", tripId).
                param("lat", 49.8445500).
                param("long", 23.9970000).
                expect().
                statusCode(200).
//                log().all().
        when().
                        post(url + urlOne + tripId + urlTwo).
                        then().
                        extract().
                        response();

        return Post.path("data.message");
    }

}
