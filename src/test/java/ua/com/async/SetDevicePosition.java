package ua.com.async;

import ua.com.Url;
import ua.com.Variables;
import ua.com.service.RandomGenerator;
import static io.restassured.RestAssured.given;

public class SetDevicePosition {
    public SetDevicePosition(String loginToken, String tripId, String lat, String lng) {
        String url = Url.base;
        String urlSetPosition = Url.setPosition;

        Variables variables = new Variables();
        RandomGenerator randomGenerator = new RandomGenerator();
        float speed = randomGenerator.floatRandomGenerator(variables.getSpeedMin(), variables.getSpeedRangeMax());


        given().
                relaxedHTTPSValidation().
                param("lat", lat).
                param("long", lng).
                param("speed", speed).
                param("bearing", "22").
                param("mode", "driver").
                header("Authorization", "Bearer " + loginToken).
                expect().
                statusCode(200).
//                log().all().
        when().
                        post(url + urlSetPosition).
                        then().
                        extract().
                        response();

        System.out.println("POINT TRIP " + tripId + " SENT");
    }
}
