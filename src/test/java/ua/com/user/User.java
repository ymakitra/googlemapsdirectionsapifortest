package ua.com.user;

import io.restassured.response.Response;
import ua.com.Url;
import static io.restassured.RestAssured.given;

public class User {
    public static void AllUsers(String loginToken, String deviceToken, String attribute, String attributeValue) {
        String url = Url.base;
        String urlTwo = Url.users;
        String sort = "DESC";
        String expandParams = "user";

        given().
                relaxedHTTPSValidation().
                header("Authorization", "Bearer " + loginToken).
                header("X-Device-Token", deviceToken).
                param("attribute", attribute).
                param("attributeValue", attributeValue).
                param("sort", sort).
                param("expandParams", expandParams).
                expect().
                statusCode(200).
//                log().all().
        when().
                get(url + urlTwo + "?:attribute=" + attributeValue + "&sort=" + sort + "&expand=" + expandParams).
                then().
                extract().
                response();
    }

    public static void DeleteUser(String loginToken, String deviceToken, int userId) {
        String url = Url.base;
        String urlTwo = Url.users_;

        given().
                relaxedHTTPSValidation().
                header("Authorization", "Bearer " + loginToken).
                header("X-Device-Token", deviceToken).
                param("id", userId).
                expect().
                statusCode(204).
//                log().all().
        when().
                delete(url + urlTwo + userId).
                then().
                extract().
                response();
    }

    public static void GetUser(String loginToken, String deviceToken, int userId) {
        String url = Url.base;
        String urlTwo = Url.users_;


        given().
                relaxedHTTPSValidation().
                header("Authorization", "Bearer " + loginToken).
                header("X-Device-Token", deviceToken).
                param("id", userId).
                expect().
                statusCode(200).
//                log().all().
        when().
                get(url + urlTwo + userId).
                then().
                extract().
                response();
    }

    public static String ResetUserActivities(String loginToken, String deviceToken) {
        String url = Url.base;
        String urlTwo = Url.reset;

        Response post;
        post = given().
                relaxedHTTPSValidation().
                header("Authorization", "Bearer " + loginToken).
                header("X-Device-Token", deviceToken).
                expect().
                statusCode(200).
//                log().all().
                when().
                post(url + urlTwo).
                then().
                extract().
                response();
        return post.path("data.message");
    }

    public static String SetRatingForUser(String loginToken, String deviceToken, int userId, int passengerTripID) {
        String url = Url.base;
        String urlTwo = Url.users_;
        String urlTree = Url.rating;

        Response post;
        post = given().
                relaxedHTTPSValidation().
                header("Authorization", "Bearer " + loginToken).
                header("X-Device-Token", deviceToken).
                param("id", userId).
                param("passenger_trip_id", passengerTripID).
                param("rating", 3).
                expect().
                statusCode(200).
//                log().all().
                when().
                post(url + urlTwo + userId + urlTree).
                then().
                extract().
                response();
        return post.path("data.message");
    }
}
