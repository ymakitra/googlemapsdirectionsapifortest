package ua.com;

public class Url {
    static public String base = "https://api.letlift.demo.relevant.software/";

    static public String setPosition = "v1/position/set";
    static public String cancel = "/cancel";

    //    Authorization
    static public String login = "v1/auth/login";
    static public String logout = "v1/auth/logout";
    static public String register =  "v1/auth/register";
    static public String changePassword =  "v1/auth/change-password";
    static public String resetPassword = "v1/auth/reset-password";
    //    Car
    static public String cars = "v1/cars";
    static public String photo = "/photo";

    //    DriverTrip
    static public String driverTrip = "v1/driver-trips";
    static public String driverTrips = "v1/driver-trips/";
    static public String activeTrip = "v1/driver-trips/active";
    static public String finishTrip = "/finish";

    //    Organization
    static public String organizationList = "v1/organizations";

    //    PassengerTrip
    static public String passengerTrip = "v1/passenger-trips";
    static public String passengerTrips = "v1/passenger-trips/";
    static public String passengerActiveTrip = "v1/passenger-trips/active";

    //    Pickup
    static public String pickup = "v1/pickup/";
    static public String enquiry = "/enquiry";
    static public String accept = "/accept";
    static public String decline = "/decline";
    static public String arrived = "/arrived";

    //    Position
    static public String getDevicePosition = "v1/position/get/";
    static public String getNearestDrivers = "v1/position/nearby?";
    static public String setDevicePosition = "v1/position/set";
    static public String setDeviceRemove = "v1/position/flush";

    //    User
    static public String users = "v1/users";
    static public String users_ = "v1/users/";
    static public String reset = "v1/users/reset";
    static public String rating = "/rating";

}

