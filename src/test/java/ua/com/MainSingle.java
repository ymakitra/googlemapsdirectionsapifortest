package ua.com;

import com.google.maps.model.LatLng;
import ua.com.async.SetDevicePosition;
import ua.com.authorization.Authorization;
import ua.com.driverTrip.DriverTrip;
import ua.com.routeParser.service.ApiManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;


public class MainSingle {
    public static void main(String[] args) throws IOException, InterruptedException {
//        LOGIN
        Variables variables = new Variables();
        String email = variables.getEmail();
        String password = variables.getPassword();

        String loginToken = Authorization.Login(email, password);
        System.out.println("M LoginToken: " + loginToken);

//        CRETE TRIP
        Variables varibles = new Variables();
        String driverDevToken = variables.getDeviceTokenOne();
        int carId = variables.carId;
        int tripId = DriverTrip.CreateTrip(varibles.getStartLat(), varibles.getStartLng(), varibles.getEndLat(), varibles.getEndLng(), loginToken, carId, driverDevToken);
        System.out.println("M TripID: " + tripId);

//        GET POINTS
        ApiManager apiManager = new ApiManager();
        List<LatLng> pointsArray = new ArrayList<LatLng>();
        pointsArray = apiManager.getGoogleDirections(varibles.getStartLat(), varibles.getStartLng(), varibles.getEndLat(), varibles.getEndLng());
        int latLngSize = pointsArray.size() - 1;
        System.out.println("List size: " + latLngSize);
        int i = 0;

//        SENT POINTS
        while (i <= latLngSize) {
            double lat = pointsArray.get(i).lat;
            double lng = pointsArray.get(i).lng;
//            System.out.println(lat1);
//            System.out.println(lng1);
//            System.out.println(latLng1);
            String tripIdS = Integer.toString(tripId);
            String latS = Double.toString(lat);
            String lngS = Double.toString(lng);
            new SetDevicePosition(loginToken, tripIdS, latS, lngS);
            System.out.println("Progress: " + i + "/" + latLngSize);
            i++;

//            TIMER
            double rangeMin = variables.getTimeMin();
            double rangeMax = variables.getTimeRangeMax();

            Random r = new Random();
            double randomValue = rangeMin + (rangeMax - rangeMin) * r.nextDouble();
            System.out.println("Timer: " + randomValue);
            TimeUnit.SECONDS.sleep((long) randomValue);
        }

//        DELETE TRIP
//        new DeleteTrip(loginToken, tripId);
    }
}
