package ua.com.position;

import io.restassured.response.Response;
import ua.com.Url;
import ua.com.service.RandomGenerator;
import static io.restassured.RestAssured.given;

public class Position {

    public static String SetDevicePosition(String loginToken, String lat, String lng, float speedMin, float speedMax) {
        String url = Url.base;
        String urlSetPosition = Url.setDevicePosition;
        float speed = RandomGenerator.floatRandomGenerator(speedMin, speedMax);

        Response post = given().
                relaxedHTTPSValidation().
                header("Authorization", "Bearer " + loginToken).
                param("lat", lat).
                param("long", lng).
                param("speed", speed).
                param("bearing", "22").
                param("mode", "driver").
                expect().
                statusCode(200).
//                log().all().
        when().
                        post(url + urlSetPosition).
                        then().
                        extract().
                        response();
        return post.path("data.message");
    }

    public static int GetDevicePosition(String userId) {
        String url = Url.base;
        String urlTwo = Url.getDevicePosition;

        given().
                relaxedHTTPSValidation().
                param("id", userId).
                expect().
                statusCode(200).
//                log().all().
        when().
                get(url + urlTwo + userId).
                then().
                extract().
                response();
        return 0;
    }

    public static int GetNearestDrivers(String lat, String lng) {
        String url = Url.base;
        String urlTwo = Url.getNearestDrivers;

        given().
                relaxedHTTPSValidation().
                param("lat", lat).
                param("long", lng).
                expect().
                statusCode(200).
//                log().all().
                when().
                get(url + urlTwo + "lat=" + lat + "&long=" + lng).
                then().
                extract().
                response();
        return 0;
    }

    public static String RemoteDevicePosition(String token) {
        String url = Url.base;
        String urlTwo = Url.setDeviceRemove;

        Response post = given().
                relaxedHTTPSValidation().
                header("Authorization", "Bearer " + token).
                expect().
                statusCode(200).
//                log().all().
        when().
                        post(url + urlTwo).
                        then().
                        extract().
                        response();
        return post.path("data.message");
    }
}
