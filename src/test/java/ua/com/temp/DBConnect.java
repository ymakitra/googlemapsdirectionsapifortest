package ua.com.temp;
// "jdbc:postgresql://letlift.demo.relevant.software:5432/letliftdb", "letlift",
//  "6xc3wirZ");

import java.sql.*;
import java.util.Properties;

public class DBConnect {
    public static void main(String[] args) throws SQLException {

        // create three connections to three different databases on localhost
        Statement stmt;
        Connection conn3;

        // Connect method #3
        String dbURL3 = "jdbc:postgresql://letlift.demo.relevant.software:5432/letliftdb";
        Properties parameters = new Properties();
        parameters.put("user", "letlift");
        parameters.put("password", "6xc3wirZ");

        conn3 = DriverManager.getConnection(dbURL3, parameters);
        if (conn3 != null) {
            System.out.println("Connected to database #3");

            stmt = conn3.createStatement();
            String sql = "SELECT token FROM user_password_reset WHERE user_id =13";
//                stmt.executeUpdate(sql);

            String result;
            if (stmt.execute(sql)) {
                //ResultSet Available
                ResultSet rs = stmt.getResultSet();
                rs.next();
                result = rs.getString(1);
            } else {
                //Update count or no result available
                result = String.valueOf(stmt.getUpdateCount());
            }
            System.out.println(result);
            stmt.close();
        }
    }
}




