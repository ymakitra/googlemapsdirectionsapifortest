package ua.com;

import com.google.maps.model.LatLng;
import ua.com.service.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MainMulti2 {
    public static void main(String[] args) throws IOException, InterruptedException, NullPointerException {

//        Number Of Drivers
        VariablesMulti variablesMulti = new VariablesMulti();
        int numberOfDrivers = variablesMulti.numberOfDrivers;
        System.out.println("Number of drivers: " + numberOfDrivers);

//        LOGIN (get driver token)
        ArrayList<String> loginTokenList = Loop.TokenList(numberOfDrivers);
        System.out.println("loginTokenList: " + loginTokenList);

//        CRETE TRIP (get TRIP ID)
        ArrayList<Integer> tripIdList = Loop.TripId(numberOfDrivers, loginTokenList);
        System.out.println("tripIdList: " + tripIdList);

//        GET POINTS (get POINTS OF TRIP)
        AllUsers allUsersTrip = new AllUsers();
        List<Integer> pointsListSize = new ArrayList<Integer>();
        for (int i = 0; i < numberOfDrivers; i++) {
            List<LatLng> tripPointsList = Loop.GetPoints(i);
            pointsListSize.add(tripPointsList.size());  //!!!

            Users userTrip = new Users(tripPointsList);
            allUsersTrip.addUser(userTrip);             //!!!
            System.out.println("List " + i + ": " + userTrip.toString());
        }
        System.out.println("Size of Arrays: " + pointsListSize);

//        GET MAX LIST SIZE FOR LOOP
        int maxListSize = MaxValue.GetMaxListSize((ArrayList) pointsListSize);
        System.out.println("Max Size of Array: " + maxListSize);
        System.out.println("==============================================================================");

//        SENT POINTS
        Thread DriverTrip = null;
        for (int i = 0; i < numberOfDrivers; i++) {
            DriverTrip = new Thread(new SentDriverPositionThread(i, maxListSize, allUsersTrip, loginTokenList, tripIdList));
            DriverTrip.start();
        }

//        FINISH TRIP
        Loop.FinishTrip(numberOfDrivers, loginTokenList, tripIdList);
    }
}
